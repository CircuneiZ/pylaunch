Basically, to work this script needs Python 3.x and PyQt4.

Under Ubuntu, those can be installed with this line:
  sudo apt-get install python3 python3-pyqt4

After that the launcher can be run by executing "pylaunch" file.