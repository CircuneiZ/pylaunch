#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import builtins;
import os;
import sys;
sys.mybase = os.path.dirname(os.path.abspath(__file__));
sys.path.append(sys.mybase);

from PyQt4 import QtGui, QtCore;
from forms.mainwindow import MainWindow;

def escapeHtml(string):
	return string.replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;").replace("\"", "&quot;");
builtins.escapeHtml = escapeHtml;

def main():
	app = QtGui.QApplication(sys.argv);
	mainWindow = MainWindow();
	mainWindow.showMaximized();
	app.exec();

if __name__ == "__main__":
	sys.exit(main());