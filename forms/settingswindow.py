from PyQt4 import QtGui, QtCore;

from data import settings;
from forms.widgets import serverlist;
import sys;

from collections import OrderedDict;

class SettingsWindow(QtGui.QDialog):
	def onSettingUseMaster(self, state):
		self.setting_MasterAddress.setEnabled(state == QtCore.Qt.Checked);
	def generateMasterServerSettings(self):
		w = QtGui.QWidget(parent=self);
		l = QtGui.QVBoxLayout();
		head = QtGui.QLabel();
		head.setText("<h2>Server browser / Master server</h2>");
		l.addWidget(head);
		
		# part 1: master server ip
		gb_masterip = QtGui.QGroupBox(parent=w);
		gb_masterip.setFlat(False);
		gb_masterip.setTitle("");
		gb_masterip_l = QtGui.QVBoxLayout();
		gb_masterip_l.setMargin(4);
		gb_masterip_l.setSpacing(2);
		gb_masterip.setLayout(gb_masterip_l);
		gb_master_use = QtGui.QCheckBox(parent=gb_masterip);
		gb_master_use.setChecked(settings.GetBool("pyl.browser.master_use"));
		gb_master_use.setEnabled(False);
		gb_master_use.setText("Use the master server");
		self.setting_UseMaster = gb_master_use;
		self.connect(gb_master_use, QtCore.SIGNAL("stateChanged(int)"), self.onSettingUseMaster);
		gb_masterip_label = QtGui.QLabel(parent=gb_masterip);
		gb_masterip_label.setText("<b>Master server address</b>:");
		gb_masterip_l.addWidget(gb_masterip_label);
		gb_masterip_text = QtGui.QLineEdit(parent=gb_masterip);
		gb_masterip_text.setText("%s:%d" % (settings.Get("pyl.browser.master_address"), settings.GetInt("pyl.browser.master_port")));
		self.setting_MasterAddress = gb_masterip_text;
		self.setting_MasterAddress.setEnabled(gb_master_use.isChecked());
		gb_masterip_l.addWidget(gb_masterip_text);
		gb_masterip_l.addWidget(gb_master_use);
		l.addWidget(gb_masterip);
		
		# part 2: refresh timeouts
		gb_timeouts = QtGui.QGroupBox(parent=w);
		gb_timeouts.setFlat(False);
		gb_timeouts.setTitle("");
		gb_timeouts_l = QtGui.QHBoxLayout();
		gb_timeouts_l.setMargin(4);
		gb_timeouts_l.setSpacing(8);
		gb_timeouts.setLayout(gb_timeouts_l);
		
		# part 2.1: master timeout
		gb_timeouts_1 = QtGui.QWidget(parent=gb_timeouts);
		gb_timeouts_1_l = QtGui.QVBoxLayout();
		gb_timeouts_1_l.setMargin(0);
		gb_timeouts_1_l.setSpacing(2);
		gb_timeouts_1.setLayout(gb_timeouts_1_l);
		gb_timeouts_1_label = QtGui.QLabel(parent=gb_timeouts_1);
		gb_timeouts_1_label.setText("<b>Master refresh timeout</b>:");
		gb_timeouts_1_spin = QtGui.QSpinBox(parent=gb_timeouts_1);
		gb_timeouts_1_spin.setMinimum(100);
		gb_timeouts_1_spin.setMaximum(10000);
		gb_timeouts_1_spin.setValue(settings.GetInt("pyl.browser.timeout_master"));
		self.setting_TimeoutMaster = gb_timeouts_1_spin;
		gb_timeouts_1_l.addWidget(gb_timeouts_1_label);
		gb_timeouts_1_l.addWidget(gb_timeouts_1_spin);
		
		# part 2.2: servers timeout
		gb_timeouts_2 = QtGui.QWidget(parent=gb_timeouts);
		gb_timeouts_2_l = QtGui.QVBoxLayout();
		gb_timeouts_2_l.setMargin(0);
		gb_timeouts_2_l.setSpacing(2);
		gb_timeouts_2.setLayout(gb_timeouts_2_l);
		gb_timeouts_2_label = QtGui.QLabel(parent=gb_timeouts_2);
		gb_timeouts_2_label.setText("<b>Servers refresh timeout</b>:");
		gb_timeouts_2_spin = QtGui.QSpinBox(parent=gb_timeouts_2);
		gb_timeouts_2_spin.setMinimum(100);
		gb_timeouts_2_spin.setMaximum(10000);
		gb_timeouts_2_spin.setValue(settings.GetInt("pyl.browser.timeout_servers"));
		self.setting_TimeoutServers = gb_timeouts_2_spin;
		gb_timeouts_2_l.addWidget(gb_timeouts_2_label);
		gb_timeouts_2_l.addWidget(gb_timeouts_2_spin);
		
		gb_timeouts_l.addWidget(gb_timeouts_1);
		gb_timeouts_l.addWidget(gb_timeouts_2);
		
		l.addWidget(gb_timeouts);
		
		l.addStretch(0);
		w.setLayout(l);
		self.stackedWidget.addWidget(w);
		return w;
	
	def onPingGood(self, val):
		self.setting_PingBad.setMinimum(val+1);
	def onPingBad(self, val):
		self.setting_PingGood.setMaximum(val-1);
	def generateAppearanceSettings(self):
		w = QtGui.QWidget(parent=self);
		l = QtGui.QVBoxLayout();
		head = QtGui.QLabel();
		head.setText("<h2>Server browser / Appearance</h2>");
		l.addWidget(head);
		
		# part 1: pings
		gb_pings = QtGui.QGroupBox(parent=w);
		gb_pings.setFlat(False);
		gb_pings.setTitle("Ping indicators");
		gb_pings_l = QtGui.QVBoxLayout();
		gb_pings_l.setMargin(4);
		gb_pings_l.setSpacing(2);
		gb_pings.setLayout(gb_pings_l);
		
		pingHt = QtGui.QApplication.fontMetrics().height()+5;
		
		# part 1.1: good ping (<)
		gb_pings_1 = QtGui.QWidget(parent=gb_pings);
		gb_pings_1_l = QtGui.QHBoxLayout();
		gb_pings_1_l.setMargin(0);
		gb_pings_1_l.setSpacing(4);
		gb_pings_1.setLayout(gb_pings_1_l);
		gb_pings_1_icon = QtGui.QLabel(parent=gb_pings);
		gb_pings_1_icon.setPixmap(QtGui.QPixmap.fromImage(serverlist.g_PingGood));
		gb_pings_1_icon.setFixedWidth(pingHt);
		gb_pings_1_icon.setFixedHeight(pingHt);
		gb_pings_1_icon.setScaledContents(True);
		gb_pings_1_l.addWidget(gb_pings_1_icon);
		gb_pings_1_label = QtGui.QLabel(parent=gb_pings);
		gb_pings_1_label.setText("<b>Good ping</b> (max):");
		gb_pings_1_l.addWidget(gb_pings_1_label);
		gb_pings_1_spin = QtGui.QSpinBox(parent=gb_pings);
		gb_pings_1_spin.setMinimum(1);
		gb_pings_1_spin.setMaximum(settings.GetInt("pyl.browser.ping_bad")-1);
		gb_pings_1_spin.setFixedWidth(96);
		gb_pings_1_spin.setValue(settings.GetInt("pyl.browser.ping_good"));
		self.setting_PingGood = gb_pings_1_spin;
		self.connect(self.setting_PingGood, QtCore.SIGNAL("valueChanged(int)"), self.onPingGood);
		gb_pings_1_l.addWidget(gb_pings_1_spin);
		gb_pings_l.addWidget(gb_pings_1);
		
		# part 1.2: bad ping (<)
		gb_pings_2 = QtGui.QWidget(parent=gb_pings);
		gb_pings_2_l = QtGui.QHBoxLayout();
		gb_pings_2_l.setMargin(0);
		gb_pings_2_l.setSpacing(4);
		gb_pings_2.setLayout(gb_pings_2_l);
		gb_pings_2_icon = QtGui.QLabel(parent=gb_pings);
		gb_pings_2_icon.setPixmap(QtGui.QPixmap.fromImage(serverlist.g_PingBad));
		gb_pings_2_icon.setFixedWidth(pingHt);
		gb_pings_2_icon.setFixedHeight(pingHt);
		gb_pings_2_icon.setScaledContents(True);
		gb_pings_2_l.addWidget(gb_pings_2_icon);
		gb_pings_2_label = QtGui.QLabel(parent=gb_pings);
		gb_pings_2_label.setText("<b>Acceptable ping</b> (max):");
		gb_pings_2_l.addWidget(gb_pings_2_label);
		gb_pings_2_spin = QtGui.QSpinBox(parent=gb_pings);
		gb_pings_2_spin.setMinimum(settings.GetInt("pyl.browser.ping_good")+1);
		gb_pings_2_spin.setMaximum(1000);
		gb_pings_2_spin.setFixedWidth(96);
		gb_pings_2_spin.setValue(settings.GetInt("pyl.browser.ping_bad"));
		self.setting_PingBad = gb_pings_2_spin;
		self.connect(self.setting_PingBad, QtCore.SIGNAL("valueChanged(int)"), self.onPingBad);
		gb_pings_2_l.addWidget(gb_pings_2_spin);
		gb_pings_l.addWidget(gb_pings_2);
		
		l.addWidget(gb_pings);
		
		cb_animated = QtGui.QCheckBox(parent=w);
		cb_animated.setText("Animated refresh (note: may cause significant CPU usage increase)");
		cb_animated.setChecked(settings.GetBool("pyl.browser.refresh_animated"));
		l.addWidget(cb_animated);
		self.setting_RefreshAnimated = cb_animated;
		
		l.addStretch(0);
		w.setLayout(l);
		self.stackedWidget.addWidget(w);
		return w;
	
	def generateDescriptionWidget(self, title, text=None):
		dummyWidget = QtGui.QWidget(parent=self);
		dummyLayout = QtGui.QVBoxLayout();
		dummyLabel = QtGui.QLabel();
		dummyLabel.setText("<h2>"+escapeHtml(title)+"</h2>");
		dummyLayout.addWidget(dummyLabel);
		if text is not None:
			dummyLabel2 = QtGui.QLabel();
			dummyLabel2.setText(text);
			dummyLabel2.setWordWrap(True);
			dummyLayout.addWidget(dummyLabel2);
		dummyLayout.addStretch(0);
		dummyWidget.setLayout(dummyLayout);
		self.stackedWidget.addWidget(dummyWidget);
		return dummyWidget;
	
	def generateUpDownList(self, content=[], addbutton="Add search URL", parent=None, local=False):
		w = parent;
		wHor1 = QtGui.QWidget(parent=w);
		wHor1L = QtGui.QHBoxLayout();
		wHor1L.setMargin(0);
		wHor1L.setSpacing(3);
		wHor1.setLayout(wHor1L);
		
		wVert11 = QtGui.QWidget(parent=wHor1);
		wVert11L = QtGui.QVBoxLayout();
		wVert11L.setMargin(0);
		wVert11L.setSpacing(3);
		wVert11.setLayout(wVert11L);
		
		wVert12 = QtGui.QWidget(parent=wHor1);
		wVert12L = QtGui.QVBoxLayout();
		wVert12L.setMargin(0);
		wVert12L.setSpacing(3);
		wVert12.setLayout(wVert12L);
		
		wHor1L.addWidget(wVert11);
		wHor1L.addWidget(wVert12);
		
		w_Search_URL = QtGui.QLineEdit(parent=wVert11);
		wVert11L.addWidget(w_Search_URL);
		
		w_Search_AddC = QtGui.QWidget(parent=wVert12);
		w_Search_AddCL = QtGui.QHBoxLayout();
		w_Search_AddCL.setMargin(0);
		w_Search_AddCL.setSpacing(3);
		w_Search_AddC.setLayout(w_Search_AddCL);
		if local:
			w_Search_Browse = QtGui.QPushButton(parent=w_Search_AddC);
			w_Search_Browse.setText("...");
			w_Search_Browse.setEnabled(True);
			w_Search_AddCL.addWidget(w_Search_Browse);
		else:
			w_Search_Browse = None;
		w_Search_Add = QtGui.QPushButton(parent=w_Search_AddC);
		w_Search_Add.setText(addbutton);
		w_Search_Add.setEnabled(False);
		w_Search_AddCL.addWidget(w_Search_Add);
		wVert12L.addWidget(w_Search_AddC);
		w_Search_URLs = QtGui.QListWidget(parent=wVert11);
		w_Search_URLs.setSelectionMode(QtGui.QAbstractItemView.ExtendedSelection);
		wVert11L.addWidget(w_Search_URLs);
		w_Search_URLs_Up = QtGui.QPushButton(parent=wVert12);
		w_Search_URLs_Up.setText("Move up");
		w_Search_URLs_Up.setEnabled(False);
		wVert12L.addWidget(w_Search_URLs_Up);
		w_Search_URLs_Down = QtGui.QPushButton(parent=wVert12);
		w_Search_URLs_Down.setText("Move down");
		w_Search_URLs_Down.setEnabled(False);
		wVert12L.addWidget(w_Search_URLs_Down);
		w_Search_URLs_Remove = QtGui.QPushButton(parent=wVert12);
		w_Search_URLs_Remove.setText("Remove");
		w_Search_URLs_Remove.setEnabled(False);
		wVert12L.addWidget(w_Search_URLs_Remove);
		wVert12L.addStretch(0);
		
		# making use of Python's magic scopes
		# C++ sucks here as well, btw
		def onSearchURLChanged(text):
			if not text.strip():
				w_Search_Add.setEnabled(False);
			else:
				w_Search_Add.setEnabled(True);
		
		def recalcSelection():
			w_Search_URLs_Up.setEnabled(len(w_Search_URLs.selectedItems()));
			w_Search_URLs_Down.setEnabled(len(w_Search_URLs.selectedItems()));
			w_Search_URLs_Remove.setEnabled(len(w_Search_URLs.selectedItems()));
		
		def recalcBold():
			if not local:
				recalcSelection();
				return;
			for i in range(0, w_Search_URLs.count()):
				item = w_Search_URLs.item(i);
				font = item.font();
				font.setBold(i == 0);
				item.setFont(font);
			recalcSelection();
		
		def onSearchAddClicked():
			url = w_Search_URL.text().strip().lower();
			# search for existing item with this text
			for i in range(0, w_Search_URLs.count()):
				_url = w_Search_URLs.item(i).text().strip().lower();
				if _url == url:
					item = w_Search_URLs.takeItem(i);
					w_Search_URLs.insertItem(0, item);
					w_Search_URL.setText("");
					recalcBold();
					return;
			if url:
				w_Search_URLs.insertItem(0, url);
				w_Search_URL.setText("");
			else:
				w_Search_Add.setEnabled(False);
			recalcBold();
		
		def onSearchUpClicked():
			items = w_Search_URLs.selectedItems();
			if len(items) == 0:
				return;
			topmostIndex = -1;
			for j in range(0, w_Search_URLs.count()):
				for item in items:
					cRow = w_Search_URLs.row(item);
					if cRow != j:
						continue;
					item = w_Search_URLs.takeItem(cRow); # lol
					w_Search_URLs.insertItem(max(cRow-1, topmostIndex+1), item);
					if cRow-1 == topmostIndex:
						topmostIndex = cRow-1;
			for item in items:
				item.setSelected(True);
			recalcBold();
		
		def onSearchDownClicked():
			items = w_Search_URLs.selectedItems();
			if len(items) == 0:
				return;
			lastIndex = w_Search_URLs.count();
			for j in reversed(range(0, w_Search_URLs.count())):
				for item in items:
					cRow = w_Search_URLs.row(item);
					if cRow != j:
						continue;
					item = w_Search_URLs.takeItem(cRow); # lol (x2)
					w_Search_URLs.insertItem(min(cRow+1, lastIndex-1), item);
					if cRow+1 == lastIndex:
						lastIndex = cRow+1;
			for item in items:
				item.setSelected(True);
			recalcBold();
		
		def onSearchRemoveClicked():
			items = w_Search_URLs.selectedItems();
			if len(items) == 0:
				return;
			cIndex0 = w_Search_URLs.count();
			for item in items:
				cRow = w_Search_URLs.row(item);
				cIndex0 = min(cRow, cIndex0);
				w_Search_URLs.takeItem(cRow);
			if w_Search_URLs.count() > 0:
				w_Search_URLs.item(min(w_Search_URLs.count()-1, cIndex0)).setSelected(True);
			recalcBold();
			
		def onSearchBrowseClicked():
			_dir = str(QtGui.QFileDialog.getExistingDirectory(self, "Select directory"));
			if _dir:
				w_Search_URL.setText(_dir);
			
		for item in content:
			w_Search_URLs.addItem(item);
		recalcBold();
		
		self.connect(w_Search_URL, QtCore.SIGNAL("textChanged(QString)"), onSearchURLChanged);
		self.connect(w_Search_Add, QtCore.SIGNAL("clicked()"), onSearchAddClicked);
		self.connect(w_Search_URLs, QtCore.SIGNAL("itemSelectionChanged()"), recalcSelection);
		self.connect(w_Search_URLs_Up, QtCore.SIGNAL("clicked()"), onSearchUpClicked);
		self.connect(w_Search_URLs_Down, QtCore.SIGNAL("clicked()"), onSearchDownClicked);
		self.connect(w_Search_URLs_Remove, QtCore.SIGNAL("clicked()"), onSearchRemoveClicked);
		
		if w_Search_Browse is not None:
			self.connect(w_Search_Browse, QtCore.SIGNAL("clicked()"), onSearchBrowseClicked);
		
		wHor1.items = w_Search_URLs;
		return wHor1;
	
	def generatePWADSearching(self):
		w = QtGui.QWidget(parent=self);
		l = QtGui.QVBoxLayout();
		head = QtGui.QLabel();
		head.setText("<h2>WAD searching</h2>");
		head2 = QtGui.QLabel();
		head2.setText("<h2 style=\"margin-bottom: 0\">WAD storage</h2><i>First directory (bold) is used to save all newly downloaded WADs.</i>");

		# here we have 6 controls in the following form:
		#
		# WAD search URLs:
		# [                               ] <Add search URL>
		#
		# |-------------------------------| <Move up>
		# |                               | <Move down>
		# |-------------------------------| <Remove>
		#
		# ===========================================================
		# WAD storage directories:
		# [                               ] <Add storage directory>
		#
		# |-------------------------------| <Move up>
		# |                               | <Move down>
		# |-------------------------------| <Remove>
		
		wHor1 = self.generateUpDownList(addbutton="Add search URL", parent=w, local=False, content=settings.Get("pyl.wad.searching"));
		self.setting_WADSearchURLs = wHor1.items;
		
		wSep = QtGui.QFrame(parent=w);
		wSep.setFrameShape(QtGui.QFrame.HLine);
		wSep.setFrameShadow(QtGui.QFrame.Sunken);
		wSep.setFixedHeight(3);
		
		wHor2 = self.generateUpDownList(addbutton="Add storage path", parent=w, local=True, content=settings.Get("pyl.wad.saving"));
		self.setting_WADStoragePaths = wHor2.items;
		
		l.addWidget(head);
		l.addWidget(wHor1);
		l.addWidget(wSep);
		l.addWidget(head2);
		l.addWidget(wHor2);
		l.addStretch(0);
		w.setLayout(l);
		self.stackedWidget.addWidget(w);
		return w;
	
	def generateSettingsPages(self):
		dummyWidget = self.generateDescriptionWidget("This page doesn't exist.")
		
		pages = OrderedDict([("Server browser",
					OrderedDict([("<self>", self.generateDescriptionWidget("Server browser", "<p>This category contains settings related to server browser features, e.g. the master server address or ping display settings.</p>")),
					 ("Master server", self.generateMasterServerSettings()),
					 ("Appearance", self.generateAppearanceSettings())
					 ])),
					 ("WAD searching&storage", self.generatePWADSearching()),
				]);
		
		self.treeWidget.setUpdatesEnabled(False);
		def recursiveTree(node, widget):
			_i = 0;
			for page in node:
				if page.lower() == '<self>':
					widget.setData(QtCore.Qt.UserRole, 0, node[page]);
					continue;
				item = QtGui.QTreeWidgetItem(widget);
				item.setText(0, page);
				if isinstance(node[page], (dict)):
					recursiveTree(node[page], item);
					item.setData(QtCore.Qt.UserRole, 0, dummyWidget);
				else:
					item.setData(QtCore.Qt.UserRole, 0, dummyWidget if node[page] is None else node[page]);
				widget.insertChild(_i, item);
				item.setExpanded(True);
				_i += 1;
		
		_i = 0;
		for page in pages:
			item = QtGui.QTreeWidgetItem();
			item.setText(0, page);
			if isinstance(pages[page], (dict)):
				item.setData(QtCore.Qt.UserRole, 0, dummyWidget);
				recursiveTree(pages[page], item);
			else:
				item.setData(QtCore.Qt.UserRole, 0, dummyWidget if pages[page] is None else pages[page]);
			self.treeWidget.insertTopLevelItem(_i, item);
			#QTreeWidgetItem::DontShowIndicator
			item.setExpanded(True);
			#item.setChildIndicatorPolicy(QtGui.QTreeWidgetItem.DontShowIndicatorWhenChildless);
			_i += 1;
		self.treeWidget.setUpdatesEnabled(True);
		self.connect(self.treeWidget, QtCore.SIGNAL("currentItemChanged(QTreeWidgetItem *,QTreeWidgetItem *)"), self.treeItemSelected);
	
	def treeItemSelected(self, new, old):
		self.stackedWidget.setCurrentWidget(new.data(QtCore.Qt.UserRole, 0));
	
	def __init__(self, parent=None):
		super(SettingsWindow, self).__init__(parent);
		# This is the most complicated dialog. No, really. Even compared to the server browser. The server browser is just hacky.
		#central = QtGui.QWidget(parent=self);
		central = self;
		layout = QtGui.QVBoxLayout();
		self.setLayout(layout);
		self.setWindowTitle("PyLaunch Settings");
		#self.setCentralWidget(central);
		
		self.setFixedWidth(640);
		self.setFixedHeight(480);
		
		layout.setSpacing(3);
		layout.setMargin(4);
		
		widget_mainArea = QtGui.QWidget(parent=central);
		layout_mainArea = QtGui.QHBoxLayout();
		layout_mainArea.setMargin(0);
		layout_mainArea.setSpacing(3);
		self.treeWidget = QtGui.QTreeWidget(parent=widget_mainArea);
		self.treeWidget.setFixedWidth(190);
		self.treeWidget.header().hide();
		layout_mainArea.addWidget(self.treeWidget);
		self.stackedWidget = QtGui.QStackedWidget(parent=widget_mainArea);
		layout_mainArea.addWidget(self.stackedWidget);
		widget_mainArea.setLayout(layout_mainArea);
		layout.addWidget(widget_mainArea);
		
		sep = QtGui.QFrame(parent=central);
		sep.setObjectName("line");
		sep.setFixedHeight(3);
		sep.setFrameShape(QtGui.QFrame.HLine);
		sep.setFrameShadow(QtGui.QFrame.Sunken);
		layout.addWidget(sep);
		
		widget_bottomButtons = QtGui.QWidget(parent=central);
		layout_bottomButtons = QtGui.QHBoxLayout();
		layout_bottomButtons.addStretch(0);
		self.buttonOk = QtGui.QPushButton(parent=widget_bottomButtons);
		self.buttonOk.setDefault(True);
		self.buttonOk.setAutoDefault(True);
		self.buttonOk.setText("Apply");
		layout_bottomButtons.setMargin(0);
		layout_bottomButtons.setSpacing(3);
		layout_bottomButtons.addWidget(self.buttonOk);
		self.buttonCancel = QtGui.QPushButton(parent=widget_bottomButtons);
		self.buttonCancel.setText("Cancel");
		layout_bottomButtons.addWidget(self.buttonCancel);
		widget_bottomButtons.setLayout(layout_bottomButtons);
		layout.addWidget(widget_bottomButtons);
	
		self.connect(self.buttonOk, QtCore.SIGNAL("clicked()"), self.onButtonOk);
		self.connect(self.buttonCancel, QtCore.SIGNAL("clicked()"), self.onButtonCancel);
		
		self.generateSettingsPages();
	
	def onButtonCancel(self):
		self.close();
	
	def onButtonOk(self):
		# setting_MasterAddress.text()
		# setting_UseMaster.isChecked()
		# setting_TimeoutMaster.value()
		# setting_TimeoutServers.value()
		_masterA = "";
		_masterP = 15300;
		__masterText = self.setting_MasterAddress.text().strip();
		if ':' in __masterText:
			s = __masterText.split(':');
			_masterA = s[0];
			try:
				p = int(s[1]);
			except:
				pass;
		else:
			_masterA = __masterText;
		settings.Set("pyl.browser.master_address", _masterA);
		settings.Set("pyl.browser.master_port", _masterP);
		settings.Set("pyl.browser.master_use", self.setting_UseMaster.isChecked());
		settings.Set("pyl.browser.timeout_master", self.setting_TimeoutMaster.value());
		settings.Set("pyl.browser.timeout_servers", self.setting_TimeoutServers.value());
		settings.Set("pyl.browser.ping_bad", self.setting_PingBad.value());
		settings.Set("pyl.browser.ping_good", self.setting_PingGood.value());
		settings.Set("pyl.browser.refresh_animated", self.setting_RefreshAnimated.isChecked());
		_items = [];
		for i in range(0, self.setting_WADSearchURLs.count()):
			_items.append(self.setting_WADSearchURLs.item(i).text());
		settings.Set("pyl.wad.searching", _items);
		_items = [];
		for i in range(0, self.setting_WADStoragePaths.count()):
			_items.append(self.setting_WADStoragePaths.item(i).text());
		settings.Set("pyl.wad.saving", _items);
		self.close();