from PyQt4 import QtGui, QtCore;

from data import settings;
from data import zandronum;

import forms.widgets.serverlist;

from forms.widgets.playerlist import PlayerTable, checkLightness;

GlobalWindows = [];

class ServerWindow(QtGui.QDialog):
	@classmethod
	def generateFor(self, parent, server):
		global GlobalWindows;
		for window in GlobalWindows:
			if window.addr[0] == server.addr[0] and window.addr[1] == server.addr[1]:
				return window;
		sWnd = self(parent);
		GlobalWindows.append(sWnd);
		return sWnd;
	
	@classmethod
	def setRefreshEnabled(self, enabled):
		global GlobalWindows;
		for window in GlobalWindows:
			window.sRefreshButton.setEnabled(enabled);
	
	@classmethod
	def findFor(self, server):
		global GlobalWindows;
		for window in GlobalWindows:
			if window.addr[0] == server.addr[0] and window.addr[1] == server.addr[1]:
				return window;
		return None;
	
	def generateTeamLabel(self, team, text, right):
		wid = QtGui.QLabel(parent=self.head);
		wid.setTextFormat(QtCore.Qt.PlainText);
		wid.setText(text);
		pal = wid.palette();
		backColor = QtGui.QColor(team.color|0xFF000000);
		pal.setColor(QtGui.QPalette.Window, backColor);
		tbr = checkLightness(backColor);
		if tbr < 128:
			foreColor = QtGui.QColor(0xFFFFFFFF);
		else:
			foreColor = QtGui.QColor(0xFF000000);
		pal.setColor(QtGui.QPalette.WindowText, foreColor);
		wid.setPalette(pal);
		wid.setBackgroundRole(QtGui.QPalette.Window);
		wid.setAutoFillBackground(True);
		wid.setContentsMargins(QtCore.QMargins(3, 3, 3+(3 if right else 0), 3));
		fon = wid.font();
		fon.setBold(True);
		fon.setPointSize(fon.pointSize()+0.5);
		if right:
			wid.setAlignment(QtCore.Qt.AlignRight);
		else:
			wid.setAlignment(QtCore.Qt.AlignLeft);
		wid.setFont(fon);
		return wid;
	
	def __init__(self, parent=None):
		super(ServerWindow, self).__init__(parent);
		
		self.addr = None;
		
		# 1. divide the window into two halves: base info and DMFlags*/CompatFlags* details
		central = QtGui.QWidget(parent=self);
		cLayout = QtGui.QVBoxLayout();
		cLayout.setMargin(8);
		cLayout.setSpacing(8);
		cLayout.addWidget(central);
		self.setLayout(cLayout);
		layout = QtGui.QHBoxLayout();
		layout.setMargin(0);
		layout.setSpacing(6);
		central.setLayout(layout);
		self.setWindowTitle("Server details");
		self.setFixedWidth(740);
		
		# 2. second half, dmflags details, consists of exactly one widget
		sDMFlagsLayout = QtGui.QVBoxLayout();
		self.sDMFlagsLayout = sDMFlagsLayout;
		
		sDMFlagsContainer = QtGui.QWidget(parent=central);
		sDMFlagsContainer.setLayout(sDMFlagsLayout);
		self.sDMFlagsContainer = sDMFlagsContainer;
		sDMFlags = QtGui.QLabel(parent=sDMFlagsContainer);
		self.sDMFlags = sDMFlags;
		sDMFlagsLayout.addWidget(sDMFlags);
		sDMFlagsLayout.addStretch(0);
		
		sGenericBoldFont = self.font();
		sGenericBoldFont.setBold(True);
		
		sVertSepr = QtGui.QFrame(parent=central);
		sVertSepr.setFrameShape(QtGui.QFrame.VLine);
		sVertSepr.setFrameShadow(QtGui.QFrame.Sunken);
		sVertSepr.setFixedWidth(3);
		
		# 3. first half, server details, is just another complex layout generated programmatically ;(
		sInfo = QtGui.QWidget(parent=central);
		# 3.1. top part (server name, game mode, ping, country)
		sServerTitle = QtGui.QLabel(parent=sInfo);
		sServerTitle.setTextFormat(QtCore.Qt.PlainText);
		sServerTitleFont = sServerTitle.font();
		sServerTitleFont.setPointSize(sServerTitleFont.pointSize()+1);
		sServerTitleFont.setBold(True);
		sServerTitle.setFont(sServerTitleFont);
		sServerTitle.setText("Server title");
		self.sServerTitle = sServerTitle;
		# ----- #
		sServerGameMode = QtGui.QLabel(parent=sInfo);
		sServerGameModeFont = sServerGameMode.font();
		sServerGameModeFont.setItalic(True);
		sServerGameMode.setFont(sServerGameModeFont);
		sServerGameMode.setTextFormat(QtCore.Qt.PlainText);
		sServerGameMode.setText("Game mode");
		self.sServerGameMode = sServerGameMode;
		# ----- #
		pingHt = QtGui.QApplication.fontMetrics().height()+2;
		sServerMiscHead = QtGui.QWidget(parent=sInfo);
		sServerPingTitle = QtGui.QLabel(parent=sServerMiscHead);
		sServerPingTitle.setFont(sGenericBoldFont);
		sServerPingTitle.setText("Ping: ");
		self.sServerPingTitle = sServerPingTitle;
		sServerPingPixmap = QtGui.QLabel(parent=sServerMiscHead);
		sServerPingPixmap.setFixedWidth(pingHt);
		sServerPingPixmap.setFixedHeight(pingHt);
		sServerPingPixmap.setScaledContents(True);
		self.sServerPingPixmap = sServerPingPixmap;
		sServerPingValue = QtGui.QLabel(parent=sServerMiscHead);
		sServerPingValue.setTextFormat(QtCore.Qt.PlainText);
		sServerPingValue.setText("—");
		sServerPingValue.setContentsMargins(0, 0, 16, 0);
		self.sServerPingValue = sServerPingValue;
		sServerCountryTitle = QtGui.QLabel(parent=sServerMiscHead);
		sServerCountryTitle.setFont(sGenericBoldFont);
		sServerCountryTitle.setText("Country: ");
		self.sServerCountryTitle = sServerCountryTitle;
		sServerCountryPixmap = QtGui.QLabel(parent=sServerMiscHead);
		flagH = pingHt - 4;
		flagW = flagH * 1.5;
		sServerCountryPixmap.setFixedWidth(flagW+4);
		sServerCountryPixmap.setFixedHeight(flagH);
		sServerCountryPixmap.setScaledContents(True);
		sServerCountryPixmap.setContentsMargins(0, 0, 4, 0);
		self.sServerCountryPixmap = sServerCountryPixmap;
		sServerCountryValue = QtGui.QLabel(parent=sServerMiscHead);
		sServerCountryValue.setTextFormat(QtCore.Qt.PlainText);
		sServerCountryValue.setText("N/A");
		self.sServerCountryValue = sServerCountryValue;
		# ----- #
		sServerMiscNeck = QtGui.QFrame(parent=sInfo);
		sServerMiscNeck.setFrameShape(QtGui.QFrame.HLine);
		sServerMiscNeck.setFrameShadow(QtGui.QFrame.Sunken);
		# 3.2. middle part (timelimit, pointlimit, fraglimit, winlimit)
		sServerLimitsContainer = QtGui.QWidget(parent=sInfo);
		sServerLimitsCol1 = QtGui.QWidget(parent=sServerLimitsContainer);
		sServerLimitsCol2 = QtGui.QWidget(parent=sServerLimitsContainer);
		sServerTimeLimit = QtGui.QLabel(parent=sServerLimitsCol1);
		sServerTimeLimit.setText("<b>Time limit:</b>\t—");
		self.sServerTimeLimit = sServerTimeLimit;
		sServerPointLimit = QtGui.QLabel(parent=sServerLimitsCol2);
		sServerPointLimit.setText("<b>Point limit:</b>\t—");
		self.sServerPointLimit = sServerPointLimit;
		sServerFragLimit = QtGui.QLabel(parent=sServerLimitsCol1);
		sServerFragLimit.setText("<b>Frag limit:</b>\t—");
		self.sServerFragLimit = sServerFragLimit;
		sServerWinLimit = QtGui.QLabel(parent=sServerLimitsCol2);
		sServerWinLimit.setText("<b>Win limit:</b>\t—");
		self.sServerWinLimit = sServerWinLimit;
		# 3.3. optional middle part (teams)
		sServerTeamsSep = QtGui.QFrame(parent=sInfo);
		sServerTeamsSep.setFrameShape(QtGui.QFrame.HLine);
		sServerTeamsSep.setFrameShadow(QtGui.QFrame.Sunken);
		self.sServerTeamsSep = sServerTeamsSep;
		
		# 3.4. middle part (players)
		sServerPlayersSep = QtGui.QFrame(parent=sInfo);
		sServerPlayersSep.setFrameShape(QtGui.QFrame.HLine);
		sServerPlayersSep.setFrameShadow(QtGui.QFrame.Sunken);
		sServerPlayers = PlayerTable(parent=sInfo);
		self.players = sServerPlayers;
		
		# ===== #
		sServerMiscHeadLayout = QtGui.QHBoxLayout();
		sServerMiscHeadLayout.addWidget(sServerPingTitle);
		sServerMiscHeadLayout.addWidget(sServerPingPixmap);
		sServerMiscHeadLayout.addWidget(sServerPingValue);
		sServerMiscHeadLayout.addWidget(sServerCountryTitle);
		sServerMiscHeadLayout.addWidget(sServerCountryPixmap);
		sServerMiscHeadLayout.addWidget(sServerCountryValue);
		sServerMiscHeadLayout.addStretch(0);
		sServerMiscHeadLayout.setMargin(0);
		sServerMiscHeadLayout.setSpacing(1);
		sServerMiscHead.setLayout(sServerMiscHeadLayout);
		self.sServerMiscHeadLayout = sServerMiscHeadLayout;
		
		sServerLimitsLayout = QtGui.QHBoxLayout();
		sServerLimitsLayout.setMargin(0);
		sServerLimitsLayout.setSpacing(3);
		sServerLimitsCol1Layout = QtGui.QVBoxLayout();
		sServerLimitsCol1Layout.setMargin(0);
		sServerLimitsCol1Layout.setSpacing(3);
		sServerLimitsCol2Layout = QtGui.QVBoxLayout();
		sServerLimitsCol2Layout.setMargin(0);
		sServerLimitsCol2Layout.setSpacing(3);
		sServerLimitsCol1Layout.addWidget(sServerTimeLimit);
		sServerLimitsCol1Layout.addWidget(sServerFragLimit);
		sServerLimitsCol2Layout.addWidget(sServerPointLimit);
		sServerLimitsCol2Layout.addWidget(sServerWinLimit);
		sServerLimitsLayout.addWidget(sServerLimitsCol1);
		sServerLimitsLayout.addWidget(sServerLimitsCol2);
		sServerLimitsContainer.setLayout(sServerLimitsLayout);
		sServerLimitsCol1.setLayout(sServerLimitsCol1Layout);
		sServerLimitsCol2.setLayout(sServerLimitsCol2Layout);
		
		# -- this part was ported from PlayerList -- #
		head = QtGui.QWidget(parent=central);
		headLayout = QtGui.QHBoxLayout();
		headLayout.setMargin(0);
		headLayout.setSpacing(0);
		self.head = head;
		self.headLayout = headLayout;
		self.head.setLayout(self.headLayout);
		
		self.head1 = QtGui.QWidget(parent=head);
		self.headLayout1 = QtGui.QVBoxLayout();
		self.head1.setLayout(self.headLayout1);
		self.headLayout1.setMargin(0);
		self.headLayout1.setSpacing(0);
		self.headLayout.addWidget(self.head1);
		
		self.head2 = QtGui.QWidget(parent=head);
		self.headLayout2 = QtGui.QVBoxLayout();
		self.head2.setLayout(self.headLayout2);
		self.headLayout2.setMargin(0);
		self.headLayout2.setSpacing(0);
		self.headLayout.addWidget(self.head2);
		# -/ this part was ported from PlayerList /- #
		
		sInfoLayout = QtGui.QVBoxLayout();
		sInfoLayout.setMargin(0);
		sInfoLayout.setSpacing(4);
		sInfoLayout.addWidget(sServerTitle);
		sInfoLayout.addWidget(sServerGameMode);
		sInfoLayout.addWidget(sServerMiscHead);
		sInfoLayout.addWidget(sServerMiscNeck);
		sInfoLayout.addWidget(sServerLimitsContainer);
		sInfoLayout.addWidget(sServerTeamsSep);
		sInfoLayout.addWidget(head);
		sInfoLayout.addWidget(sServerPlayersSep);
		sInfoLayout.addWidget(sServerPlayers);
		sInfoLayout.addStretch(0);
		sInfo.setLayout(sInfoLayout);
		self.sInfoLayout = sInfoLayout;
		
		sInfo.setFixedWidth(390);
		sDMFlags.setFixedWidth(320);
		sDMFlagsLayout.setMargin(0);
		layout.addWidget(sInfo);
		layout.addWidget(sVertSepr);
		layout.addWidget(sDMFlagsContainer);
		
		# -- LOWER BUTTONS (Join, RCON, Refresh, Close) -- #
		self.sJoinButton = QtGui.QPushButton(parent=self);
		self.sJoinButton.setText("Join");
		self.sJoinButton.setEnabled(False);
		self.sRCONButton = QtGui.QPushButton(parent=self);
		self.sRCONButton.setText("RCON");
		self.sRCONButton.setEnabled(False);
		self.sRefreshButton = QtGui.QPushButton(parent=self);
		self.sRefreshButton.setText("Refresh");
		self.sCloseButton = QtGui.QPushButton(parent=self);
		self.sCloseButton.setText("Close");
		self.connect(self.sCloseButton, QtCore.SIGNAL("clicked()"), self.closeClicked);
		self.connect(self.sRefreshButton, QtCore.SIGNAL("clicked()"), self.refreshClicked);
		
		self.sButtonsArea = QtGui.QWidget(parent=self);
		self.sButtonsLayout = QtGui.QHBoxLayout();
		self.sButtonsLayout.setMargin(0);
		cLayout.addWidget(self.sButtonsArea);
		self.sMainLayout = cLayout;
		
		self.sButtonsLayout.addStretch(0);
		self.sButtonsLayout.addWidget(self.sJoinButton);
		self.sButtonsLayout.addWidget(self.sRCONButton);
		self.sButtonsLayout.addWidget(self.sRefreshButton);
		self.sButtonsLayout.addWidget(self.sCloseButton);
		self.sButtonsArea.setLayout(self.sButtonsLayout);
	
	def setServer(self, server):
		if server.addr is None or server.addr[0] is None or server.addr[1] is None:
			self.close();
		
		self.addr = (server.addr[0], server.addr[1]);
		
		self.setWindowTitle("Server details for %s:%d" % (server.addr[0], server.addr[1]));
		
		self.sServerTitle.setText(server.title);
		self.sServerGameMode.setText(server.game_mode_as_string());
		if server.ping != 0:
			if server.na:
				self.sServerPingValue.setText("%d (N/A)" % (server.ping));
			else:
				self.sServerPingValue.setText(str(server.ping));
		else:
			self.sServerPingValue.setText("N/A");
			
		if server.ping == 0:
			if server.refreshing:
				led = forms.widgets.serverlist.g_PingRefresh[0];
			else:
				led = forms.widgets.serverlist.g_PingNA;
		else:
			if server.refreshing:
				led = forms.widgets.serverlist.g_PingRefresh[0];
			elif server.na:
				if server.ping < settings.GetInt("pyl.browser.ping_good"):
					led = forms.widgets.serverlist.g_PingNGood;
				elif server.ping < settings.GetInt("pyl.browser.ping_bad"):
					led = forms.widgets.serverlist.g_PingNBad;
				else:
					led = forms.widgets.serverlist.g_PingNTerrible;
			else:
				if server.ping < settings.GetInt("pyl.browser.ping_good"):
					led = forms.widgets.serverlist.g_PingGood;
				elif server.ping < settings.GetInt("pyl.browser.ping_bad"):
					led = forms.widgets.serverlist.g_PingBad;
				else:
					led = forms.widgets.serverlist.g_PingTerrible;
		self.sServerPingPixmap.setPixmap(QtGui.QPixmap.fromImage(led));
			
		if server.country is not None:
			self.sServerCountryValue.setText(server.country[1]);
			if server.flag is None:
				self.sServerCountryPixmap.hide();
			else:
				self.sServerCountryPixmap.show();
				if server.flag not in forms.widgets.serverlist.g_Flags:
					forms.widgets.serverlist.g_Flags[server.flag] = QtGui.QImage("%s/img/flags/%s.png" % (sys.mybase, server.flag));
					#print("refresh: Loading image for %s." % (server.flag.upper()));
				flag = forms.widgets.serverlist.g_Flags[server.flag];
				self.sServerCountryPixmap.setPixmap(QtGui.QPixmap.fromImage(flag));
		
		_dmflags = "";
		
		if server.game_mode is None or server.ping == 0:
			_dmflags += "<b>The server is unreachable and/or invalid.</b>";
		
		if server.iwad:
			_dmflags += "<b>IWAD:</b><br>";
			_dmflags += "%s<br>" % (server.iwad.lower());
			_dmflags += "<br>";
		
		if len(server.pwads) > 0:
			_dmflags += "<b>PWADs:</b><br>";
			for pwad in server.pwads:
				_dmflags += "%s<br>" % (pwad.lower());
			_dmflags += "<br>";
		
		if server.forcepassword or server.forcejoinpassword:
			_dmflags += "<b>Flags:</b><br>";
			if server.forcepassword:
				_dmflags += "Has connect password<br>";
			if server.forcejoinpassword:
				_dmflags += "Has join password<br>";
			_dmflags += "<br>";
		if server.dmflags != 0:
			_dmflags += "<b>DMFlags (%d):</b><br>" % (server.dmflags);
			for dmflag in zandronum.ZandronumFlags.mDMFlags:
				if (server.dmflags & dmflag) == dmflag:
					_dmflags += "%s [%d]<br>" % (zandronum.ZandronumFlags.mDMFlags[dmflag], dmflag);
			_dmflags += "<br>";
		if server.dmflags2 != 0:
			_dmflags += "<b>DMFlags2 (%d):</b><br>" % (server.dmflags2);
			for dmflag in zandronum.ZandronumFlags.mDMFlags2:
				if (server.dmflags2 & dmflag) == dmflag:
					_dmflags += "%s [%d]<br>" % (zandronum.ZandronumFlags.mDMFlags2[dmflag], dmflag);
			_dmflags += "<br>";
		if server.dmflags3 != 0:
			_dmflags += "<b>DMFlags3 (%d):</b><br>" % (server.dmflags3);
			for dmflag in zandronum.ZandronumFlags.mDMFlags3:
				if (server.dmflags3 & dmflag) == dmflag:
					_dmflags += "%s [%d]<br>" % (zandronum.ZandronumFlags.mDMFlags3[dmflag], dmflag);
			_dmflags += "<br>";
		if server.compatflags != 0:
			_dmflags += "<b>CompatFlags (%d):</b><br>" % (server.compatflags);
			for dmflag in zandronum.ZandronumFlags.mCpFlags:
				if (server.compatflags & dmflag) == dmflag:
					_dmflags += "%s [%d]<br>" % (zandronum.ZandronumFlags.mCpFlags[dmflag], dmflag);
			_dmflags += "<br>";
		if server.compatflags2 != 0:
			_dmflags += "<b>CompatFlags2 (%d):</b><br>" % (server.compatflags2);
			for dmflag in zandronum.ZandronumFlags.mCpFlags2:
				if (server.compatflags2 & dmflag) == dmflag:
					_dmflags += "%s [%d]<br>" % (zandronum.ZandronumFlags.mCpFlags2[dmflag], dmflag);
			_dmflags += "<br>";
		self.sDMFlags.setText(_dmflags);
		
		if server.timelimit != 0:
			self.sServerTimeLimit.setText("<b>Time limit:</b>\t%d" % (server.timelimit));
		else:
			self.sServerTimeLimit.setText("<b>Time limit:</b>\t—");
		
		if server.pointlimit != 0:
			self.sServerPointLimit.setText("<b>Point limit:</b>\t%d" % (server.pointlimit));
		else:
			self.sServerPointLimit.setText("<b>Point limit:</b>\t—");
		
		if server.fraglimit != 0:
			self.sServerFragLimit.setText("<b>Frag limit:</b>\t%d" % (server.fraglimit));
		else:
			self.sServerFragLimit.setText("<b>Frag limit:</b>\t—");
		
		if server.winlimit != 0:
			self.sServerWinLimit.setText("<b>Win limit:</b>\t%d" % (server.winlimit));
		else:
			self.sServerWinLimit.setText("<b>Win limit:</b>\t—");
		
		players = sorted(server.players);
		teams = server.teams;
		
		self.players.setHorizontalHeaderLabels(['', 'Nickname', '%ss' % (server.point_name()), 'Ping', 'Time']);
		
		self.players.server = server;
		self.server = server;
		
		self.players.teams = teams;
		self.players.setRowCountEx(len(players));
		self.players.setSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Expanding);
		
		_i = 0;
		if len(self.players.teams) > 0:
			for i in range(0, len(self.players.teams)):
				for player in players:
					if player.spectator or player.team != i:
						continue;
					self.players.createRow(_i, player);
					_i += 1;
		else:
			for player in players:
				if player.spectator:
					continue;
				self.players.createRow(_i, player);
				_i += 1;
		for player in players:
			if not player.spectator:
				continue;
			self.players.createRow(_i, player);
			_i += 1;
		
		if len(self.players.teams) > 0:
			while self.headLayout1.count() > 0:
				wid = self.headLayout1.takeAt(0);
				wid.widget().deleteLater();
			while self.headLayout2.count() > 0:
				wid = self.headLayout2.takeAt(0);
				wid.widget().deleteLater();
			
			for team in self.players.teams:
				self.headLayout1.addWidget(self.generateTeamLabel(team, team.name, False));
				self.headLayout2.addWidget(self.generateTeamLabel(team, ('%d / %d' % (team.score, server.pointlimit)) if server.pointlimit > 0 else str(team.score), True));
			self.head.show();
			self.sServerTeamsSep.show();
		else:
			self.head.hide();
			self.sServerTeamsSep.hide();
	
	def show(self):
		self.sInfoLayout.invalidate();
		self.sInfoLayout.activate();
		self.headLayout1.invalidate();
		self.headLayout1.activate();
		self.headLayout2.invalidate();
		self.headLayout2.activate();
		self.headLayout.invalidate();
		self.headLayout.activate();
		self.sDMFlagsLayout.invalidate();
		self.sDMFlagsLayout.activate();
		self.repaint();
		super(ServerWindow, self).show();
		self.activateWindow();
	
	def closeEvent(self, event):
		global GlobalWindows;
		GlobalWindows.remove(self);
		super(ServerWindow, self).closeEvent(event);
		
	def closeClicked(self):
		self.close();

	def refreshClicked(self):
		self.parent().tryRefresh(self.server);
