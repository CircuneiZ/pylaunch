from PyQt4 import QtGui, QtCore;

from data import settings;
from forms.settingswindow import SettingsWindow;
from forms.serverwindow import ServerWindow;
from data import zandronum;
from forms.widgets.serverlist import ServerList;
from forms.widgets import playerlist;
import sys;

class MainWindow(QtGui.QMainWindow):
	def __init__(self):
		super(MainWindow, self).__init__();
		
		self.afterRefresh_openServer = None;
		self.afterRefresh_openGame = None;
		
		# initialize main window
		self.resize(800, 600);
		self.setWindowTitle("PyLaunch");
		self.mainWidget = QtGui.QWidget(parent=self);
		self.setCentralWidget(self.mainWidget);
		
		# create menu bar
		self.menuBar = self.menuBar();#QtGui.QMenuBar(parent=self);
		self.menuBar_pylaunch = QtGui.QMenu(parent=self.menuBar);
		self.menuBar_pylaunch.setTitle("PyLaunch");
		self.menuBar.addMenu(self.menuBar_pylaunch);
		self.connect(self.menuBar_pylaunch.addAction("Settings"), QtCore.SIGNAL("triggered(bool)"), self.onSettings);
		self.menuBar_pylaunch.addSeparator();
		self.connect(self.menuBar_pylaunch.addAction("Quit"), QtCore.SIGNAL("triggered(bool)"), self.onQuit);
		self.menuBar.setNativeMenuBar(False);
		#self.setMenuBar(self.menuBar);
		
		# create main layout (vertical, contains only tab widget right now)
		_mainVBox = QtGui.QVBoxLayout();
		_mainVBox.setMargin(4);
		_mainVBox.setSpacing(2);
		self.mainWidget.setLayout(_mainVBox);
		
		# create main working area (the tab widget)
		self.mainTabs = QtGui.QTabWidget(parent=self);
		self.mainTabs_servers = QtGui.QWidget(parent=self.mainTabs);
		self.mainTabs.addTab(self.mainTabs_servers, "Servers");
		_mainVBox.addWidget(self.mainTabs);

		# create servers query window
		_serversVBox = QtGui.QVBoxLayout();
		_serversVBox.setMargin(3);
		_serversVBox.setSpacing(3);
		self.mainTabs_servers.setLayout(_serversVBox);
		
		# main panel
		self.mainTabs_servers_panel = QtGui.QFrame(parent=self.mainTabs_servers);
		self.mainTabs_servers_panel.setFixedHeight(38);
		self.mainTabs_servers_panel.setFrameShape(QtGui.QFrame.NoFrame);
		self.mainTabs_servers_panel.setFrameShadow(QtGui.QFrame.Raised);
		
		# refresh servers button
		_panelHBox = QtGui.QHBoxLayout();
		_panelHBox.setMargin(0);
		_panelHBox.setSpacing(1);
		self.mainTabs_servers_panel.setLayout(_panelHBox);
		verticalSeparator = QtGui.QFrame(parent=self.mainTabs_servers_panel);
		verticalSeparator.setFrameShape(QtGui.QFrame.VLine);
		verticalSeparator.setFrameShadow(QtGui.QFrame.Sunken);
		verticalSeparator.setFixedWidth(3);
		verticalSeparator.setFixedHeight(28);
		buttonLaunch = QtGui.QPushButton(parent=self.mainTabs_servers_panel);
		buttonLaunch.setFixedWidth(32);
		buttonLaunch.setFixedHeight(32);
		buttonLaunch.setFlat(True);
		buttonLaunch.setIcon(QtGui.QIcon("%s/img/zandronum.png" % (sys.mybase)));
		buttonLaunch.setIconSize(QtCore.QSize(24, 24));
		buttonLaunch.setToolTip("Launch Zandronum");
		self.connect(buttonLaunch, QtCore.SIGNAL("clicked()"), self.launchClicked);
		buttonLaunch.setEnabled(False);
		self.mainTabs_servers_panel_launch = buttonLaunch;
		buttonRefresh = QtGui.QPushButton(parent=self.mainTabs_servers_panel);
		buttonRefresh.setFixedWidth(32);
		buttonRefresh.setFixedHeight(32);
		buttonRefresh.setFlat(True);
		buttonRefresh.setIcon(QtGui.QIcon("%s/img/refreshAll.png" % (sys.mybase)));
		buttonRefresh.setIconSize(QtCore.QSize(24, 24));
		buttonRefresh.setToolTip("Refresh all servers");
		self.mainTabs_servers_panel_refresh = buttonRefresh;
		self.connect(buttonRefresh, QtCore.SIGNAL("clicked()"), self.refreshClicked);
		buttonRefreshOne = QtGui.QPushButton(parent=self.mainTabs_servers_panel);
		buttonRefreshOne.setFixedWidth(32);
		buttonRefreshOne.setFixedHeight(32);
		buttonRefreshOne.setFlat(True);
		buttonRefreshOne.setIcon(QtGui.QIcon("%s/img/refresh.png" % (sys.mybase)));
		buttonRefreshOne.setIconSize(QtCore.QSize(24, 24));
		buttonRefreshOne.setToolTip("Refresh selected server");
		self.connect(buttonRefreshOne, QtCore.SIGNAL("clicked()"), self.refreshOneClicked);
		self.mainTabs_servers_panel_refreshOne = buttonRefreshOne;
		buttonServerInfo = QtGui.QPushButton(parent=self.mainTabs_servers_panel);
		buttonServerInfo.setFixedWidth(32);
		buttonServerInfo.setFixedHeight(32);
		buttonServerInfo.setFlat(True);
		buttonServerInfo.setIcon(QtGui.QIcon("%s/img/serverInfo.png" % (sys.mybase)));
		buttonServerInfo.setIconSize(QtCore.QSize(24, 24));
		buttonServerInfo.setToolTip("Server details");
		self.connect(buttonServerInfo, QtCore.SIGNAL("clicked()"), self.serverInfoClicked);
		self.mainTabs_servers_panel_serverInfo = buttonServerInfo;
		self.refresher = zandronum.ZandronumRefresher();
		self.connect(self.refresher, QtCore.SIGNAL("initialRefreshFinished()"), self.initialRefreshFinished);
		self.connect(self.refresher, QtCore.SIGNAL("refreshProgress(QString,int,int,int)"), self.refreshProgress);
		self.connect(self.refresher, QtCore.SIGNAL("refreshFinished()"), self.refreshFinished);
		_panelHBox.addWidget(buttonLaunch);
		_panelHBox.addWidget(verticalSeparator);
		_panelHBox.addWidget(buttonRefresh);
		_panelHBox.addWidget(buttonRefreshOne);
		_panelHBox.addWidget(buttonServerInfo);
		_panelHBox.addStretch();
		
		_serversVBox.addWidget(self.mainTabs_servers_panel);
		
		# servers list
		self.mainTabs_servers_list = ServerList(parent=self.mainTabs_servers);
		self.mainTabs_servers_list._window = self;
		_serversVBox.addWidget(self.mainTabs_servers_list);
		
		# servers search
		self.mainTabs_servers_find = QtGui.QWidget(parent=self.mainTabs_servers);
		findLayout = QtGui.QHBoxLayout();
		self.mainTabs_servers_find.setLayout(findLayout);
		findLabel = QtGui.QLabel(parent=self.mainTabs_servers_find);
		findLabel.setText("Dynamic filter: ");
		self.mainTabs_servers_find_text = QtGui.QLineEdit(parent=self.mainTabs_servers_find);
		findLayout.setMargin(0);
		findLayout.setSpacing(2);
		findLayout.addWidget(findLabel);
		findLayout.addWidget(self.mainTabs_servers_find_text);
		#findNext = QtGui.QPushButton(parent=self.mainTabs_servers_find);
		#findNext.setFlat(False);
		#findNext.setText("Find next");
		#findNext.resize(findNext.sizeHint());
		#findPrev = QtGui.QPushButton(parent=self.mainTabs_servers_find);
		#findPrev.setFlat(False);
		#findPrev.setText("Find previous");
		#findPrev.resize(findPrev.sizeHint());
		#findLayout.addWidget(findPrev);
		#findLayout.addWidget(findNext);
		""" we're not using find prev/next since the search works as filter now """
		findLayout.addStretch();
		self.mainTabs_servers_find.hide();
		self.connect(self.mainTabs_servers_find_text, QtCore.SIGNAL("textEdited(QString)"), self.findChanged);
		_serversVBox.addWidget(self.mainTabs_servers_find);
		
		# timer
		self.timer = QtCore.QTimer(parent=self);
		self.timer.setInterval(250);
		self.connect(self.timer, QtCore.SIGNAL("timeout()"), self.timerTick);
		self.refresh_frame = 0;
		self.timer.start();
		
		# statusbar
		sbar = QtGui.QStatusBar();
		self.sbar = sbar;
		self.setStatusBar(sbar);
		sbar_prog = QtGui.QProgressBar();
		sbar_prog.setMinimum(0);
		sbar_prog.setMaximum(1);
		sbar_prog.setValue(0);
		sbar_prog.setFixedWidth(120);
		self.sbar_prog = sbar_prog;
		self.sbar_prog.hide();
		self.sbar.showMessage("Started.", 5000);
		self.sbar.addPermanentWidget(self.sbar_prog);
		sbar_label = QtGui.QLabel();
		sbar_label.setContentsMargins(QtCore.QMargins(16, 0, 16, 0));
		sbar_label.setText("0 servers");
		self.sbar_label = sbar_label;
		self.sbar.addPermanentWidget(self.sbar_label);
		
		QtGui.QShortcut(QtGui.QKeySequence(QtCore.Qt.CTRL + QtCore.Qt.Key_F), self.mainTabs_servers, self.findToggle);
		QtGui.QShortcut(QtGui.QKeySequence(QtCore.Qt.Key_Escape), self.mainTabs_servers, self.findHide);
		
		self.decideServerRefresh(False);
		self.connect(self.mainTabs_servers_list, QtCore.SIGNAL("itemSelectionChanged()"), self.decideServerRefresh);
	
	def findChanged(self, newText):
		for server in self.refresher.servers:
			server.hidden = not ((newText == '') or (server.title.lower().find(newText.lower()) >= 0));
		self.mainTabs_servers_list.createSortedRows(self.refresher.servers);
	
	def findFinish(self):
		self.findChanged('');
	
	def findHide(self):
		self.mainTabs_servers_find.hide();
		self.findFinish();
	
	def findToggle(self):
		if self.mainTabs_servers_find.isVisible() and self.mainTabs_servers_find_text.hasFocus():
			self.mainTabs_servers_find.hide();
			self.findFinish();
		else:
			if not self.mainTabs_servers_find.isVisible():
				self.mainTabs_servers_find.show();
				self.mainTabs_servers_find_text.setText('');
			else:
				self.mainTabs_servers_find_text.selectAll();
			self.mainTabs_servers_find_text.setFocus(True);
	
	def refreshAll(self):
		self.mainTabs_servers_list.setRowCount(0);
		self.refresher.refresh();
	
	def refreshClicked(self):
		self.mainTabs_servers_panel_refresh.setEnabled(False);
		ServerWindow.setRefreshEnabled(False);
		self.decideServerRefresh(False);
		self.refreshAll();
	
	def createRows(self):
		self.mainTabs_servers_list.setRowCount(self.refresher.visible_count());
		_i = 0;
		for server in self.refresher.servers:
			if server.hidden:
				continue;
			self.mainTabs_servers_list.createRow(_i, server);
			_i += 1;
	
	def updateChildren(self):
		for server in self.refresher.servers:
			sWnd = ServerWindow.findFor(server);
			if sWnd is not None:
				sWnd.setServer(server);
	
	def refreshFinished(self): # this is when we actually finish refreshing anything
		self.mainTabs_servers_panel_refresh.setEnabled(True);
		ServerWindow.setRefreshEnabled(True);
		self.decideServerRefresh(True);
		#self.mainTabs_servers_list.setUpdatesEnabled(False);
		#self.createRows();
		#self.mainTabs_servers_list.setUpdatesEnabled(True);
		#self.mainTabs_servers_list.sortServers();
		self.mainTabs_servers_list.createSortedRows(self.refresher.servers);
		self.sbar_label.setText("%d servers" % (len(self.refresher.servers)));
		self.updateChildren();
		
		if self.afterRefresh_openServer is not None:
			sWnd = ServerWindow.generateFor(self, self.afterRefresh_openServer);
			sWnd.setServer(self.afterRefresh_openServer);
			sWnd.setModal(False);
			sWnd.show();
			self.afterRefresh_openServer = None;
		
		if self.afterRefresh_openGame is not None:
			# todo: either launch PWAD downloader or the game
			self.afterRefresh_openGame = None;
	
	def initialRefreshFinished(self): # this is when we get list from master server
		#self.mainTabs_servers_list.setUpdatesEnabled(False);
		#self.createRows();
		#self.mainTabs_servers_list.setUpdatesEnabled(True);
		#self.mainTabs_servers_list.sortServers();
		self.mainTabs_servers_list.createSortedRows(self.refresher.servers);
		self.sbar_label.setText("%d servers" % (len(self.refresher.servers)));
		self.updateChildren();
	
	def onSettings(self, checked):
		sWnd = SettingsWindow(parent=self);
		sWnd.setModal(True);
		sWnd.setWindowModality(QtCore.Qt.ApplicationModal);
		sWnd.show();
	
	def onQuit(self):
		self.close();
	
	def refreshOneClicked(self):
		row = self.mainTabs_servers_list.selectedRow();
		if row < 0:
			return;
		self.updateChildren();
		self.tryRefresh(self.mainTabs_servers_list._data[row]);
	
	def tryRefresh(self, server):
		if not self.mainTabs_servers_panel_refresh.isEnabled():
			return;
		self.mainTabs_servers_panel_refresh.setEnabled(False);
		ServerWindow.setRefreshEnabled(False);
		self.decideServerRefresh(False);
		server.refreshing = True;
		self.mainTabs_servers_list.viewport().update();
		self.refresher.refreshOne(server);
			
	def timerTick(self):
		if not self.mainTabs_servers_panel_refresh.isEnabled() and settings.GetBool("pyl.browser.refresh_animated"):
			self.refresh_frame += 1;
			if self.refresh_frame >= 8:
				self.refresh_frame = 0;
			self.mainTabs_servers_list.viewport().update();
	
	def refreshProgress(self, message, _min, _max, _val):
		#print(message, _min, _max, _val);
		if _min != 0 or _max != 0:
			self.sbar_prog.show();
			self.sbar_prog.setMinimum(_min);
			self.sbar_prog.setMaximum(_max);
			self.sbar_prog.setValue(_val);
			self.sbar.showMessage(message, 10000);
		else:
			self.sbar.showMessage('', 10000);
			self.sbar_prog.hide();
	
	def decideServerRefresh(self, enabled=True):
		row = self.mainTabs_servers_list.selectedRow();
		enabled = (row >= 0 and row < len(self.mainTabs_servers_list._data) and enabled and self.mainTabs_servers_panel_refresh.isEnabled());
		self.mainTabs_servers_panel_refreshOne.setEnabled(enabled);
		self.mainTabs_servers_panel_serverInfo.setEnabled(enabled);
		self.mainTabs_servers_panel_launch.setEnabled(enabled);
	
	def serverInfoClicked(self):
		row = self.mainTabs_servers_list.selectedRow();
		self.afterRefresh_openServer = self.mainTabs_servers_list._data[row];
		self.tryRefresh(self.mainTabs_servers_list._data[row]);
	
	def launchClicked(self):
		row = self.mainTabs_servers_list.selectedRow();
		self.afterRefresh_openGame = self.mainTabs_servers_list._data[row];
		self.tryRefresh(self.mainTabs_servers_list._data[row]);
