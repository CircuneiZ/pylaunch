from PyQt4 import QtGui, QtCore;
from data import zandronum, settings;
from data.zandronum import ServerListColumn;
from forms.serverwindow import ServerWindow;
import sys;
import time;

from forms.widgets import playerlist;
from forms.widgets import tooltip;

class ServerListHeader(QtGui.QHeaderView):
	def __init__(self, orientation, parent=None):
		super(ServerListHeader, self).__init__(orientation, parent);
		self.cparent = parent;
		self.lastSectionClicked = -1;
		self.playersSort = False;
		self.orientation = orientation;
		self.lastSectionHovered = -1;
		self.setMouseTracking(True);
		
	def leaveEvent(self, event):
		if event is None:
			return;
		self.lastSectionHovered = -1;
		super(ServerListHeader, self).leaveEvent(event);
	
	def mouseMoveEvent(self, event):
		if event is None:
			return;
		index = self.logicalIndexAt(event.pos());
		self.lastSectionHovered = index;
		super(ServerListHeader, self).mouseMoveEvent(event);

	def mousePressEvent(self, event):
		if event is None:
			return;
		index = self.logicalIndexAt(event.pos());
		if event.button() == QtCore.Qt.LeftButton:
			self.lastSectionClicked = index;
		else:
			self.lastSectionClicked = -1;
		super(ServerListHeader, self).mousePressEvent(event);
	
	# basically, it's impossible to sort on 1st column (flags, that is the place where we display connectpass/joinpass icons), and there's custom on-off sort on 3rd column (players)
	def mouseReleaseEvent(self, event):
		if event is None:
			return;
		index = self.logicalIndexAt(event.pos());
		if event.button() == QtCore.Qt.LeftButton and self.lastSectionClicked == index:
			if index == ServerListColumn.Players:
				self.playersSort = not self.playersSort;
				settings.Set("pyl.browser.sort_players", self.playersSort);
				self.lastSectionClicked = -1;
				oldOrder = self.sortIndicatorOrder();
				oldSection = self.sortIndicatorSection();
				super(ServerListHeader, self).mouseReleaseEvent(event);
				self.setSortIndicator(oldSection, oldOrder);
				self.cparent.sortServers();
				return;
			elif index == ServerListColumn.Flags:
				self.lastSectionClicked = -1;
				oldOrder = self.sortIndicatorOrder();
				oldSection = self.sortIndicatorSection();
				super(ServerListHeader, self).mouseReleaseEvent(event);
				self.setSortIndicator(oldSection, oldOrder);
				return;
		_change = (not self.isSortIndicatorShown());
		_change_sec = self.sortIndicatorSection();
		self.setUpdatesEnabled(False);
		super(ServerListHeader, self).mouseReleaseEvent(event);
		if (self.sortIndicatorSection() != _change_sec or _change) and self.sortIndicatorOrder() == QtCore.Qt.AscendingOrder:
			self.setSortIndicator(self.sortIndicatorSection(), QtCore.Qt.DescendingOrder); # force the first column selection to be descending and NOT ascending (also in fact it sorts the other way around in __lt__)
		settings.Set("pyl.browser.sort_column", self.sortIndicatorSection());
		settings.Set("pyl.browser.sort_order", self.sortIndicatorOrder());
		self.setUpdatesEnabled(True);
		self.cparent.sortServers();
	
	def paintSection(self, painter, rect, logicalIndex):
		if logicalIndex == ServerListColumn.Players:
			if not rect.isValid():
				return;
			opt = QtGui.QStyleOptionHeader();
			self.initStyleOption(opt);
			state = QtGui.QStyle.State_None;
			if self.isEnabled():
				state |= QtGui.QStyle.State_Enabled;
			if self.window().isActiveWindow():
				state |= QtGui.QStyle.State_Active;
			# todo: somehow determine hover and set QtGui.QStyle.State_MouseOver
			if self.lastSectionClicked == logicalIndex:
				state |= QtGui.QStyle.State_Sunken;
			if self.lastSectionHovered == logicalIndex:
				state |= QtGui.QStyle.State_MouseOver;

			if logicalIndex == ServerListColumn.Players:
				if self.playersSort:
					opt.sortIndicator = QtGui.QStyleOptionHeader.SortUp;
			elif self.isSortIndicatorShown() and self.sortIndicatorSection() == logicalIndex:
				opt.sortIndicator = QtGui.QStyleOptionHeader.SortDown if self.sortIndicatorOrder() == QtCore.Qt.AscendingOrder else QtGui.QStyleOptionHeader.SortUp;
			
			textAlignment = QtCore.Qt.AlignLeft;
			opt.rect = rect;
			opt.section = logicalIndex;
			opt.state |= state;
			opt.textAlignment = textAlignment;
			opt.iconAlignment = QtCore.Qt.AlignVCenter;
			opt.text = str(self.model().headerData(logicalIndex, self.orientation, QtCore.Qt.DisplayRole));
			visual = self.visualIndex(logicalIndex);
			if self.count() == 1:
				opt.position = QtGui.QStyleOptionHeader.OnlyOneSection;
			elif visual == 0:
				opt.position = QtGui.QStyleOptionHeader.Beginning;
			elif visual == self.count()-1:
				opt.position = QtGui.QStyleOptionHeader.End;
			else:
				opt.position = QtGui.QStyleOptionHeader.Middle;
			opt.orientation = self.orientation;
			opt.selectedPosition = QtGui.QStyleOptionHeader.NotAdjacent;
			self.style().drawControl(QtGui.QStyle.CE_Header, opt, painter, self);
			
		else:
			super(ServerListHeader, self).paintSection(painter, rect, logicalIndex);

# HI (unlike two previous cases, you can do it in C++, but it's generally not advised)
g_DoomGuyBase = QtGui.QImage("%s/img/doomguy_base.png" % (sys.mybase));
g_DoomGuyColor = QtGui.QImage("%s/img/doomguy_color.png" % (sys.mybase));
g_DoomGuyFree = QtGui.QImage("%s/img/doomguy_free.png" % (sys.mybase));
g_DoomGuyEmpty = QtGui.QImage("%s/img/doomguy_empty.png" % (sys.mybase));
g_DoomGuyKinds = {};

g_FConnect = QtGui.QImage("%s/img/flag_connectpassword.png" % (sys.mybase));
g_FJoin = QtGui.QImage("%s/img/flag_joinpassword.png" % (sys.mybase));
g_FMorePlayers = QtGui.QImage("%s/img/flag_moreplayers.png" % (sys.mybase));

g_PingGood = QtGui.QImage("%s/img/ping_good.png" % (sys.mybase));
g_PingBad = QtGui.QImage("%s/img/ping_bad.png" % (sys.mybase));
g_PingTerrible = QtGui.QImage("%s/img/ping_terrible.png" % (sys.mybase));
g_PingNA = QtGui.QImage("%s/img/ping_na.png" % (sys.mybase));

g_PingNGood = QtGui.QImage("%s/img/ping_ngood.png" % (sys.mybase));
g_PingNBad = QtGui.QImage("%s/img/ping_nbad.png" % (sys.mybase));
g_PingNTerrible = QtGui.QImage("%s/img/ping_nterrible.png" % (sys.mybase));

g_PingRefresh = [None] * 8;
for i in range(0, 8):
	g_PingRefresh[i] = QtGui.QImage("%s/img/ping_ref%d.png" % (sys.mybase, i+1));

g_Flags = {};

def getDoomGuy(kind, r, g, b):
	if kind in g_DoomGuyKinds:
		return g_DoomGuyKinds[kind];
	img = QtGui.QImage(g_DoomGuyBase.width(), g_DoomGuyBase.height(), QtGui.QImage.Format_ARGB32);
	for y in range(0, img.height()):
		for x in range(0, img.width()):
			px = g_DoomGuyBase.pixel(x, y);
			img.setPixel(x, y, px);
			px = g_DoomGuyColor.pixel(x, y);
			_r = (px & 0x00FF0000) >> 16;
			_g = (px & 0x0000FF00) >> 8;
			_b = (px & 0x000000FF);
			_a = (px & 0xFF000000) >> 24;
			if _a < 255:
				continue;
			_r = min(int(_r*(r/255)), 255);
			_g = min(int(_g*(g/255)), 255);
			_b = min(int(_b*(b/255)), 255);
			px = 0;
			px |= _a; px <<= 8;
			px |= _r; px <<= 8;
			px |= _g; px <<= 8;
			px |= _b;
			img.setPixel(x, y, px);
	g_DoomGuyKinds[kind] = img;
	return img;

class ServerListItem(QtGui.QStyledItemDelegate):
	def __init__(self, parent=None):
		super(ServerListItem, self).__init__(parent);
		self.cparent = parent;
	
	def paintSelectionBg(self, painter, option, index):
		if index.row() == self.cparent.selectedRow():
			palette = self.cparent.palette();
			qrec = QtCore.QRect(option.rect);
			color = palette.color(QtGui.QPalette.Highlight);
			color.setAlpha(64);
			painter.fillRect(qrec, color);
	
	def paintSelectionFg(self, painter, option, index):
		if index.row() == self.cparent.selectedRow():
			palette = self.cparent.palette();
			qrec = QtCore.QRect(option.rect);
			color = palette.color(QtGui.QPalette.Highlight);
			color.setAlpha(64);
			painter.fillRect(qrec, color);

	def paint(self, painter, option, index):
		palette = self.cparent.palette();
		row = self.cparent.selectedRow();
		b_textRow = False;
		b_textContent = None;
		
		#server = index.data(QtCore.Qt.UserRole);
		server = self.cparent._data[index.row()];
		if server is None or server.addr[0] is None or server.addr[1] is None:
			return;
		
		if index.column() == ServerListColumn.Address:
			b_textRow = True;
			b_textContent = "%s:%d" % (server.addr[0], server.addr[1]);
		elif index.column() == ServerListColumn.IWAD:
			b_textRow = True;
			b_textContent = server.iwad.lower();
		elif index.column() == ServerListColumn.Map:
			b_textRow = True;
			b_textContent = server.level.lower();
		elif index.column() == ServerListColumn.PWADs:
			b_textRow = True;
			b_textContent = ", ".join(server.pwads).lower();
		elif index.column() == ServerListColumn.GameMode:
			b_textRow = True;
			b_textContent = str(server.game_mode_as_string());
		elif index.column() == ServerListColumn.Players:
			self.paintSelectionBg(painter, option, index);
			b_textRow = False;
			qrec = QtCore.QRect();
			qrec.setLeft(option.rect.left()+1);
			qrec.setTop(option.rect.top());
			qrec.setBottom(option.rect.bottom());
			qrec.setRight(qrec.left()+qrec.height()*0.75+1);
			imgW_raw = (qrec.height()*0.75);
			#painter.drawImage(qrec, getDoomGuy('green', 0, 255, 0));
			s_players = 0;
			s_bots = 0;
			s_spectators = 0;
			s_free = 0;
			s_empty = 0;
			# max is 8. doubled max is 16.
			s_max = 8;
			s_scale = 1.0;
			if len(server.players) > 8:
				s_max = min(16, len(server.players));
				#s_scale = 0.5;
				s_scale = (imgW_raw*8) / (imgW_raw*s_max);
			for player in server.players:
				if player.spectator:
					s_spectators += 1;
					continue;
				if player.bot:
					s_bots += 1;
					continue;
				s_players += 1;
			s_free = server.maxplayers-len(server.players);
			s_empty = server.maxclients-len(server.players);
			if s_free < 0: s_free = 0;
			if s_empty < 0: s_empty = 0;
			s_total = 0;
			imgW = imgW_raw*s_scale;
			for i in range(0, s_players):
				s_total += 1;
				if s_total > s_max:
					break;
				qrec.setLeft(option.rect.right()-imgW-((s_total-1)*imgW));
				qrec.setRight(qrec.left()+imgW);
				painter.drawImage(qrec, getDoomGuy('green', 0, 255, 0));
			for i in range(0, s_bots):
				s_total += 1;
				if s_total > s_max:
					break;
				qrec.setLeft(option.rect.right()-imgW-((s_total-1)*imgW));
				qrec.setRight(qrec.left()+imgW);
				painter.drawImage(qrec, getDoomGuy('cyan', 0, 255, 255));
			for i in range(0, s_spectators):
				s_total += 1;
				if s_total > s_max:
					break;
				qrec.setLeft(option.rect.right()-imgW-((s_total-1)*imgW));
				qrec.setRight(qrec.left()+imgW);
				painter.drawImage(qrec, getDoomGuy('yellow', 255, 255, 0));
			for i in range(0, s_free):
				s_total += 1;
				if s_total > s_max:
					break;
				qrec.setLeft(option.rect.right()-imgW-((s_total-1)*imgW));
				qrec.setRight(qrec.left()+imgW);
				painter.drawImage(qrec, g_DoomGuyFree);
			for i in range(0, s_empty):
				s_total += 1;
				if s_total > s_max:
					break;
				qrec.setLeft(option.rect.right()-imgW-((s_total-1)*imgW));
				qrec.setRight(qrec.left()+imgW);
				painter.drawImage(qrec, g_DoomGuyEmpty);
			if index.row() == self.cparent.selectedRow():
				qrec = QtCore.QRect(option.rect);
				color = palette.color(QtGui.QPalette.Highlight);
				color.setAlpha(64);
				painter.fillRect(qrec, color);
		elif index.column() == ServerListColumn.Flags:
			self.paintSelectionBg(painter, option, index);
			b_textRow = False;
			# display:
			#  1) "more players" icon if server has >16 players
			#  2) "connect password" icon if server has it
			#  3) "join password" icon if server has it
			qrec = QtCore.QRect();
			qrec.setLeft(option.rect.left()+2);
			qrec.setRight(option.rect.right());
			qrec.setTop(option.rect.top());
			heightScalar = option.rect.height() / 24;
			qrec.setBottom(qrec.top()+int(8*heightScalar));
			if len(server.players) > 16:
				painter.drawImage(qrec, g_FMorePlayers);
			qrec.translate(0, int(8*heightScalar));
			if server.forcepassword:
				painter.drawImage(qrec, g_FConnect);
				qrec.translate(0, int(8*heightScalar));
			if server.forcejoinpassword:
				painter.drawImage(qrec, g_FJoin);
				qrec.translate(0, int(8*heightScalar));
			if index.row() == self.cparent.selectedRow():
				qrec = QtCore.QRect(option.rect);
				color = palette.color(QtGui.QPalette.Highlight);
				color.setAlpha(64);
				painter.fillRect(qrec, color);
		elif index.column() == ServerListColumn.Ping:
			# custom painting
			b_textRow = False;
			if server.ping == 0:
				if server.refreshing:
					led = g_PingRefresh[self.cparent._window.refresh_frame];
				else:
					led = g_PingNA;
				b_textContent = "n/a";
			else:
				if server.refreshing:
					led = g_PingRefresh[self.cparent._window.refresh_frame];
				elif server.na:
					if server.ping < settings.GetInt("pyl.browser.ping_good"):
						led = g_PingNGood;
					elif server.ping < settings.GetInt("pyl.browser.ping_bad"):
						led = g_PingNBad;
					else:
						led = g_PingNTerrible;
				else:
					if server.ping < settings.GetInt("pyl.browser.ping_good"):
						led = g_PingGood;
					elif server.ping < settings.GetInt("pyl.browser.ping_bad"):
						led = g_PingBad;
					else:
						led = g_PingTerrible;
				b_textContent = str(server.ping);
			if index.row() == self.cparent.selectedRow():
				painter.fillRect(option.rect, palette.color(QtGui.QPalette.Highlight));
				painter.setPen(palette.color(QtGui.QPalette.HighlightedText));
			else:
				painter.setPen(palette.color(QtGui.QPalette.Text));
			qrec = QtCore.QRect();
			qrec.setTop(option.rect.top()+2);
			qrec.setBottom(option.rect.bottom()-3);
			pingSz = qrec.height();
			qrec.setLeft(option.rect.left()+4);
			qrec.setRight(qrec.left()+pingSz);
			qrec.setTop(option.rect.top()+2);
			qrec.setBottom(qrec.top()+pingSz);
			painter.setRenderHint(QtGui.QPainter.Antialiasing, True);
			painter.setRenderHint(QtGui.QPainter.SmoothPixmapTransform, True);
			painter.setRenderHint(QtGui.QPainter.HighQualityAntialiasing, True);
			painter.drawImage(qrec, led);
			painter.setRenderHint(QtGui.QPainter.Antialiasing, False);
			painter.setRenderHint(QtGui.QPainter.SmoothPixmapTransform, False);
			painter.setRenderHint(QtGui.QPainter.HighQualityAntialiasing, False);
			if index.row() == self.cparent.selectedRow():
				qrec = QtCore.QRect(option.rect);
				color = palette.color(QtGui.QPalette.Highlight);
				color.setAlpha(64);
				painter.fillRect(qrec, color);
			qrec = QtCore.QRectF(option.rect);
			metrics = QtGui.QApplication.fontMetrics();
			qrec.setTop(qrec.top()+2);
			qrec.setBottom(qrec.bottom()-3);
			textOption = QtGui.QTextOption();
			textOption.setAlignment(QtCore.Qt.AlignCenter);
			qrec.setLeft(qrec.left()+pingSz+8);
			qrec.setRight(qrec.right()-4);
			qrec.setBottom(option.rect.bottom());
			painter.drawText(qrec, b_textContent, textOption);
		elif index.column() == ServerListColumn.Title:
			b_textRow = False;
			qrec = QtCore.QRect(option.rect);
			
			if index.row() == self.cparent.selectedRow():
				painter.fillRect(qrec, palette.color(QtGui.QPalette.Highlight));
				painter.setPen(palette.color(QtGui.QPalette.HighlightedText));
			else:
				painter.setPen(palette.color(QtGui.QPalette.Text));
			
			haveFlag = True;
			flag = None;
			if server.flag is None:
				haveFlag = False;
			else:
				if server.flag not in g_Flags:
					g_Flags[server.flag] = QtGui.QImage("%s/img/flags/%s.png" % (sys.mybase, server.flag));
					#print("refresh: Loading image for %s." % (server.flag.upper()));
				flag = g_Flags[server.flag];
			
			if haveFlag:
				flagH = qrec.height()-9;
				flagW = flagH * 1.5;
				qrec.setLeft(qrec.left()+4);
				qrec.setRight(qrec.left()+flagW);
				qrec.setTop(qrec.top()+4);
				qrec.setBottom(qrec.top()+flagH);
				painter.drawImage(qrec, flag);
				if index.row() == self.cparent.selectedRow():
					qrec = QtCore.QRect(option.rect);
					color = palette.color(QtGui.QPalette.Highlight);
					color.setAlpha(64);
					painter.fillRect(qrec, color);
				qrec = QtCore.QRect(option.rect);
			
			metrics = QtGui.QApplication.fontMetrics();
			if haveFlag:
				qrec.setLeft(qrec.left()+12+flagW);
			else:
				qrec.setLeft(qrec.left()+4);
			qrec.setTop(qrec.top()+3);
			qrec.setRight(qrec.right()-4);
			painter.drawText(qrec, 0, str(server.title));

		if b_textRow:
			qrec = QtCore.QRect(option.rect);
			metrics = QtGui.QApplication.fontMetrics();
			if index.row() == self.cparent.selectedRow():
				painter.fillRect(qrec, palette.color(QtGui.QPalette.Highlight));
				painter.setPen(palette.color(QtGui.QPalette.HighlightedText));
			else:
				painter.setPen(palette.color(QtGui.QPalette.Text));
			qrec.setLeft(qrec.left()+4);
			qrec.setTop(qrec.top()+3);
			qrec.setRight(qrec.right()-4);
			#qrec.setBottom(qrec.bottom()-4);
			painter.drawText(qrec, 0, b_textContent);

class ServerList(QtGui.QTableWidget):
	def __init__(self, parent=None):
		super(ServerList, self).__init__(parent);
		self.lastCellX = -1;
		self.lastCellY = -1;
		self.mouseX = -1;
		self.mouseY = -1;
		self._data = [];
		self.setMouseTracking(True);
		self.setDragEnabled(False);
		self.setEditTriggers(QtGui.QAbstractItemView.NoEditTriggers);
		self.setSelectionBehavior(QtGui.QAbstractItemView.SelectRows);
		self.setHorizontalHeader(ServerListHeader(QtCore.Qt.Horizontal, self));
		self.setColumnCount(9);
		# let's just set it to my own settings right now
		self.horizontalHeader().setSortIndicator(settings.GetInt("pyl.browser.sort_column"), settings.GetInt("pyl.browser.sort_order"));
		self.horizontalHeader().playersSort = settings.GetBool("pyl.browser.sort_players");
		self.setHorizontalHeaderLabels(['', 'Players', 'Ping', 'Title', 'Address', 'IWAD', 'Map', 'PWADs', 'Game mode']);
		self.horizontalHeader().setStretchLastSection(False);
		self.horizontalHeader().setMovable(False);
		self.horizontalHeader().setClickable(True);
		self.horizontalHeader().setSortIndicatorShown(True);
		self.horizontalHeader().setHighlightSections(False);
		self.horizontalHeader().setDefaultAlignment(QtCore.Qt.AlignLeft);
		metrics = QtGui.QApplication.fontMetrics();
		self.horizontalHeader().setResizeMode(ServerListColumn.Flags, QtGui.QHeaderView.Fixed);
		self.horizontalHeader().resizeSection(ServerListColumn.Flags, (metrics.height()+8));
		self.horizontalHeader().setResizeMode(ServerListColumn.Ping, QtGui.QHeaderView.Fixed);
		self.horizontalHeader().resizeSection(ServerListColumn.Ping, max(metrics.width("Ping"), metrics.width("9999")+8)+metrics.height()+8);
		self.horizontalHeader().setResizeMode(ServerListColumn.Players, QtGui.QHeaderView.Fixed);
		self.horizontalHeader().setResizeMode(ServerListColumn.Title, QtGui.QHeaderView.Stretch);
		self.horizontalHeader().setResizeMode(ServerListColumn.Address, QtGui.QHeaderView.Fixed);
		self.horizontalHeader().resizeSection(ServerListColumn.Address, metrics.width("999.999.999.999:99999")+8);
		self.horizontalHeader().setResizeMode(ServerListColumn.IWAD, QtGui.QHeaderView.Fixed);
		self.horizontalHeader().resizeSection(ServerListColumn.IWAD, metrics.width("PLUTONIA.WAD")+8);
		self.horizontalHeader().setResizeMode(ServerListColumn.Map, QtGui.QHeaderView.Fixed);
		self.horizontalHeader().resizeSection(ServerListColumn.Map, metrics.width("ZDWARS99")+8); # heh ZDWars maps are long
		self.horizontalHeader().setResizeMode(ServerListColumn.PWADs, QtGui.QHeaderView.Interactive);
		self.horizontalHeader().resizeSection(ServerListColumn.PWADs, 140);
		self.horizontalHeader().setResizeMode(ServerListColumn.GameMode, QtGui.QHeaderView.Fixed);
		self.horizontalHeader().resizeSection(ServerListColumn.GameMode, max(metrics.width("Game mode")+8, metrics.width("Team Deathmatch")+8));
		self.verticalHeader().setDefaultSectionSize(metrics.height()+8);
		# players column size is 8 * (rowHeight * 0.75)
		self.horizontalHeader().resizeSection(ServerListColumn.Players, ((metrics.height()+8) * 0.75) * 8);
		self.verticalHeader().hide();
		self.setItemDelegate(ServerListItem(self));
		#self.connect(self, QtCore.SIGNAL("cellEntered(int, int)"), self.cellEnteredEvent);
		self.setVerticalScrollMode(QtGui.QAbstractItemView.ScrollPerPixel);
		self.setHorizontalScrollMode(QtGui.QAbstractItemView.ScrollPerPixel);
	
	def mousePressEvent(self, event):
		super(ServerList, self).mousePressEvent(event);
		if event.button() == QtCore.Qt.MidButton:
			if (self.lastCellY < 0) or (self.lastCellY >= len(self._data)):
				return;
			self._window.afterRefresh_openServer = self._data[self.lastCellY];
			self._window.tryRefresh(self._data[self.lastCellY]);
	
	def selectedRow(self):
		selectedIndexes = self.selectedIndexes();
		for index in selectedIndexes:
			return index.row();
		return -1;
	
	def setRowCount(self, newCount):
		super(ServerList, self).setRowCount(newCount);
		self._data = [None] * newCount;
	
	def createRow(self, row, server):
		if len(self._data) != self.rowCount():
			self._data = [None] * self.rowCount();
		self._data[row] = server;
		#self.setSortingEnabled(False);
		#for i in range(0, self.columnCount()):
		#	#item = QtGui.QTableWidgetItem();
		#	#item.setData(QtCore.Qt.UserRole, server);
		#	#self.setItem(row, i, item);
		#self.setSortingEnabled(True);
		#pass;
	
	def sortServers(self):
		column = self.horizontalHeader().sortIndicatorSection();
		order = self.horizontalHeader().sortIndicatorOrder();
		servers = [];
		for i in range(0, self.rowCount()):
			server = self._data[i];
			if server is None:
				continue;
			#server = server.data(QtCore.Qt.UserRole);
			server.sort_column = column;
			server.sort_order = order;
			server.sort_players = self.horizontalHeader().playersSort; # HI (x2)
			servers.append(server);
		servers.sort();
		_i = 0;
		self.setUpdatesEnabled(False);
		for server in servers:
			self.createRow(_i, server);
			_i += 1;
		self.setUpdatesEnabled(True);
	
	def mouseMoveEvent(self, event):
		super(ServerList, self).mouseMoveEvent(event);
		if playerlist.g_PlayerList is None:
			playerlist.g_PlayerList = playerlist.PlayerList(parent=self._window);
		if tooltip.g_ToolTip is None:
			tooltip.g_ToolTip = tooltip.ToolTip(parent=self._window);
		pos = event.pos();
		self.mouseX = pos.x()-self.rect().x();
		self.mouseY = pos.y()-self.rect().y();
		innerPos = self.mouseY + self.verticalScrollBar().value();
		y = int(innerPos / self.verticalHeader().defaultSectionSize());
		x = 0;
		innerPosX = self.mouseX + self.horizontalScrollBar().value();
		tmpX = 0;
		for i in range(self.columnCount()):
			tmpX += self.horizontalHeader().sectionSize(i);
			if innerPosX <= tmpX:
				break;
			x += 1;
		if x != self.lastCellX or y != self.lastCellY:
			self.lastCellX = x;
			self.lastCellY = y;
			if y >= len(self._data):
				playerlist.g_PlayerList.hide();
				return;
			server = self._data[y];
			if self.mouseX < 0 or self.mouseY < 0:
				playerlist.g_PlayerList.hide();
				tooltip.g_ToolTip.hide();
				return;
			if self.lastCellX == ServerListColumn.Players:
				tooltip.g_ToolTip.hide();
				# we're displaying: teams and limits
				if (server.ping == 0) or (server.game_mode is None) or (len(server.teams) == 0 and len(server.players) == 0):
					playerlist.g_PlayerList.hide();
					return;
				#playerlist.g_PlayerList.move(playerlist.g_PlayerList.mapToParent(QtCore.QPoint(self.mouseX+1, self.mouseY-10)));
				parentPos = self._window.pos();
				#playerlist.g_PlayerList.move(self.mouseX+1-parentPos.x(), self.mouseY-10-parentPos.y());
				#playerlist.g_PlayerList.move(self.mouseX+5, self.mouseY-10);
				playerlist.g_PlayerList.move(event.globalPos().x()+12, event.globalPos().y()-8);
				playerlist.g_PlayerList.show(server);
			elif self.lastCellX == ServerListColumn.Flags:
				playerlist.g_PlayerList.hide();
				tooltip.g_ToolTip.move(event.globalPos().x()+12, event.globalPos().y()-8);
				flagData = [];
				if len(server.players) > 16:
					flagData.append('More than 16 players');
				if server.forcepassword:
					flagData.append('Has connect password');
				if server.forcejoinpassword:
					flagData.append('Has join password');
				if len(flagData) > 0:
					tooltip.g_ToolTip.show("\n".join(flagData));
				else:
					tooltip.g_ToolTip.hide();
			elif self.lastCellX == ServerListColumn.Title:
				playerlist.g_PlayerList.hide();
				tooltip.g_ToolTip.move(event.globalPos().x()+12, event.globalPos().y()-8);
				serverData = [];
				if server.ping > 0:
					serverData.append("Zandronum Server version %s" % (server.version));
					serverData.append("Country: %s" % (server.country[1]) if server.country is not None else 'Unknown');
					serverData.append("Name: %s" % (server.title));
					serverData.append("Address: %s:%d" % (server.addr[0], server.addr[1]));
					serverData.append("Game: %s" % (server.game_name));
					serverData.append("Skill: %s (%d)" % (server.skill_as_string(), server.skill));
					serverData.append("Game mode: %s%s" % (server.game_mode_as_string(), ' (instagib)' if server.instagib else ' (buckshot)' if server.buckshot else ''));
					if server.dmflags != 0:
						serverData.append("DMFlags: %d" % (server.dmflags));
					if server.dmflags2 != 0:
						serverData.append("DMFlags2: %d" % (server.dmflags2));
					if server.dmflags3 != 0:
						serverData.append("DMFlags3: %d" % (server.dmflags3));
					if server.compatflags != 0:
						serverData.append("CompatFlags: %d" % (server.compatflags));
					if server.compatflags2 != 0:
						serverData.append("CompatFlags2: %d" % (server.compatflags2));
				if len(serverData) > 0:
					tooltip.g_ToolTip.show("\n".join(serverData));
				else:
					tooltip.g_ToolTip.hide();
			elif self.lastCellX == ServerListColumn.PWADs:
				playerlist.g_PlayerList.hide();
				tooltip.g_ToolTip.move(event.globalPos().x()+12, event.globalPos().y()-8);
				if len(server.pwads) > 0:
					tooltip.g_ToolTip.show("\n".join(server.pwads));
				else:
					tooltip.g_ToolTip.hide();
			else:
				playerlist.g_PlayerList.hide();
				tooltip.g_ToolTip.hide();
	
	def leaveEvent(self, event):
		if playerlist.g_PlayerList is not None:
			playerlist.g_PlayerList.hide();
		if tooltip.g_ToolTip is not None:
			tooltip.g_ToolTip.hide();
		self.mouseX = -1;
		self.mouseY = -1;
		self.lastCellX = -1;
		self.lastCellY = -1;
		super(ServerList, self).leaveEvent(event);
	
	def contextMenuEvent(self, event):
		if (self.lastCellY < 0) or (self.lastCellY >= len(self._data)):
			return;
		self._window.tryRefresh(self._data[self.lastCellY]);
	
	def createSortedRows(self, servers):
		self.setUpdatesEnabled(False);
		visible_count = 0;
		column = self.horizontalHeader().sortIndicatorSection();
		order = self.horizontalHeader().sortIndicatorOrder();
		for server in servers:
			server.sort_column = column;
			server.sort_order = order;
			server.sort_players = self.horizontalHeader().playersSort; # HI (x2)
			if not server.hidden:
				visible_count += 1;
		servers.sort();
		self.setRowCount(visible_count);
		_i = 0;
		for server in servers:
			if server.hidden:
				continue;
			self.createRow(_i, server);
			_i += 1;
		self.setUpdatesEnabled(True);
