from PyQt4 import QtCore, QtGui;
import forms.widgets.serverlist;

class ToolTip(QtGui.QFrame):
	def __init__(self, parent=None):
		super(ToolTip, self).__init__(parent);
		l = QtGui.QHBoxLayout();
		self.setLayout(l);
		label = QtGui.QLabel(parent=self);
		self.label = label;
		l.addWidget(label);
		self.setAutoFillBackground(True);
		self.setWindowFlags(QtCore.Qt.Window|QtCore.Qt.FramelessWindowHint|QtCore.Qt.WindowStaysOnTopHint|QtCore.Qt.ToolTip);
		self.setAttribute(QtCore.Qt.WA_ShowWithoutActivating, True);
		l.setMargin(3);
		label.setWordWrap(False);
		self.layout = l;
		self.setFrameShape(QtGui.QFrame.Box);
		#self.setLineWidth(0);
		#self.setMidLineWidth(0);
		#self.setFrameStyle(QtGui.QFrame.Raised);
		label.setTextFormat(QtCore.Qt.PlainText);
		self.setBackgroundRole(QtGui.QPalette.ToolTipBase);
		self.setForegroundRole(QtGui.QPalette.ToolTipText);
	
	def show(self, text):
		if len(text) < 0:
			super(ToolTip, self).hide();
			return;
		self.label.setText(text);
		self.layout.invalidate();
		self.layout.activate();
		self.resize(self.sizeHint());
		super(ToolTip, self).show();

g_ToolTip = None;