from PyQt4 import QtCore, QtGui;
import forms.widgets.serverlist;

def checkLightness(color):
	R = float(color.red()) / 255.0;
	G = float(color.green()) / 255.0;
	B = float(color.blue()) / 255.0;
	c_t = int(min(255, (0.2126*R + 0.7152*G + 0.0722*B) * 255.0));
	return c_t;

class PlayerTableItem(QtGui.QStyledItemDelegate):
	def __init__(self, parent=None):
		super(PlayerTableItem, self).__init__(parent);
		self.cparent = parent;
	
	def paint(self, painter, option, index):
		palette = self.cparent.palette();
		b_textRow = False;
		b_textContent = None;
		#server = index.data(QtCore.Qt.UserRole);
		player = self.cparent._data[index.row()];
		if player is None:
			return;
		
		zrec = QtCore.QRect(option.rect);
		zrec.setLeft(zrec.left()-1);
		zrec.setRight(zrec.right()+1);
		zrec.setTop(zrec.top()-1);
		zrec.setBottom(zrec.bottom()+1);
		
		bg = palette.color(QtGui.QPalette.Base);
		bgL = checkLightness(bg);
		delta = 8;
		if bgL < 128:
			bg.setRed(min(255, bg.red()+delta));
			bg.setGreen(min(255, bg.green()+delta));
			bg.setBlue(min(255, bg.blue()+delta));
		else:
			bg.setRed(max(0, bg.red()-delta));
			bg.setGreen(max(0, bg.green()-delta));
			bg.setBlue(max(0, bg.blue()-delta));
		
		if not player.spectator:
			painter.fillRect(zrec, bg);
		
		if index.column() == 0:
			b_textRow = False;
			if player.team >= 0 and player.team < len(self.cparent.teams):
				qrec = QtCore.QRect(option.rect);
				qrec.setTop(qrec.top()-1);
				qrec.setBottom(qrec.bottom()+1);
				team = self.cparent.teams[player.team];
				color = QtGui.QColor(team.color | 0xFF000000);
				painter.fillRect(qrec, color);
		if index.column() == 1:
			b_textRow = False;
			#b_textContent = player.clean_name();
			qrec = QtCore.QRect(option.rect);
			qrec.setLeft(qrec.left()+2);
			qrec.setTop(qrec.top()+1);
			qrec.setBottom(qrec.bottom()-1);
			imgW = qrec.height() * 0.75;
			qrec.setRight(qrec.left()+imgW);
			if not player.spectator and not player.bot:
				img = forms.widgets.serverlist.getDoomGuy('green', 0, 255, 0);
			elif player.bot:
				img = forms.widgets.serverlist.getDoomGuy('cyan', 0, 255, 255);
			elif player.spectator:
				img = forms.widgets.serverlist.getDoomGuy('yellow', 255, 255, 0);
			painter.drawImage(qrec, img);
			qrec = QtCore.QRect(option.rect);
			metrics = QtGui.QApplication.fontMetrics();
			qrec.setLeft(qrec.left()+6+imgW);
			qrec.setTop(qrec.top()+3);
			qrec.setRight(qrec.right()-4);
			painter.drawText(qrec, 0, player.clean_name());
		elif index.column() == 2:
			b_textRow = True;
			b_textContent = str(player.points);
		elif index.column() == 3:
			b_textRow = True;
			if player.bot:
				b_textContent = '\u2014';
			else:
				b_textContent = str(player.ping);
		elif index.column() == 4:
			b_textRow = True;
			b_textContent = str(player.time);
		
		if b_textRow:
			qrec = QtCore.QRectF(option.rect);
			metrics = QtGui.QApplication.fontMetrics();
			qrec.setLeft(qrec.left()+4);
			qrec.setTop(qrec.top()+3);
			qrec.setRight(qrec.right()-3);
			textOption = QtGui.QTextOption();
			textOption.setAlignment(QtCore.Qt.AlignCenter);
			painter.drawText(qrec, b_textContent, textOption);

class PlayerTable(QtGui.QTableWidget):
	def __init__(self, parent=None):
		super(PlayerTable, self).__init__(parent);
		self.verticalHeader().hide();
		self.setItemDelegate(PlayerTableItem(self));
		self._data = [];
		self.teams = [];
		self.setColumnCount(5);
		metrics = QtGui.QApplication.fontMetrics();
		self.rowH = metrics.height()+8;
		self.verticalHeader().setDefaultSectionSize(self.rowH);
		self.horizontalHeader().setResizeMode(0, QtGui.QHeaderView.Fixed);
		self.horizontalHeader().resizeSection(0, 8);
		self.horizontalHeader().setResizeMode(1, QtGui.QHeaderView.Stretch);
		self.horizontalHeader().setResizeMode(2, QtGui.QHeaderView.Fixed);
		self.horizontalHeader().resizeSection(2, max(metrics.width('Points')+16, 32));
		self.horizontalHeader().setResizeMode(3, QtGui.QHeaderView.Fixed);
		self.horizontalHeader().resizeSection(3, max(metrics.width('Ping')+16, 32));
		self.horizontalHeader().setResizeMode(4, QtGui.QHeaderView.Fixed);
		self.horizontalHeader().resizeSection(4, max(metrics.width('Time')+16, 32));
		self.horizontalHeader().setHighlightSections(False);
		self.horizontalHeader().setClickable(False);
		self.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff);
		self.setGridStyle(QtCore.Qt.NoPen);
		self.setMouseTracking(True);
		self.setDragEnabled(False);
		self.setEditTriggers(QtGui.QAbstractItemView.NoEditTriggers);
		self.setSelectionBehavior(QtGui.QAbstractItemView.SelectRows);
		
	def setRowCount(self, newCount):
		super(PlayerTable, self).setRowCount(newCount);
		self._data = [None] * newCount;
		#self.setFixedHeight(self.rowHeight * newCount);
		metrics = QtGui.QApplication.fontMetrics();
		h = self.horizontalHeader().height() + 4;
		h += (metrics.height()+8) * newCount;
		self.setFixedHeight(h);
		
	def setRowCountEx(self, newCount):
		super(PlayerTable, self).setRowCount(newCount);
		self._data = [None] * newCount;
		#self.setFixedHeight(self.rowHeight * newCount);
		cResize = 16;
		if newCount > 16 and newCount <= 32:
			cResize = newCount;
		elif newCount > 32:
			cResize = 32;
		metrics = QtGui.QApplication.fontMetrics();
		h = self.horizontalHeader().height() + 4;
		h += (metrics.height()+8) * cResize;
		self.setFixedHeight(h);
		
	def createRow(self, row, player):
		if len(self._data) != self.rowCount():
			self._data = [None] * self.rowCount();
		self._data[row] = player;

class PlayerList(QtGui.QDialog):
	def __init__(self, parent=None):
		super(PlayerList, self).__init__(parent);
		self.setFixedWidth(360);
		self.setAutoFillBackground(True);
		central = QtGui.QFrame(parent=self);
		_l = QtGui.QVBoxLayout();
		_l.setMargin(0);
		_l.setSpacing(0);
		self.setLayout(_l);
		_l.addWidget(central);
		self.layout = _l;
		self.teams = [];
		
		l = QtGui.QVBoxLayout();
		l.setMargin(6);
		l.setSpacing(4);
		central.setLayout(l);
		central.setFrameShape(QtGui.QFrame.StyledPanel);
		central.setFrameShadow(QtGui.QFrame.Raised);
		
		self.setWindowFlags(QtCore.Qt.Window|QtCore.Qt.FramelessWindowHint|QtCore.Qt.WindowStaysOnTopHint|QtCore.Qt.ToolTip);
		self.setAttribute(QtCore.Qt.WA_ShowWithoutActivating, True);
		#head = QtGui.QLabel(parent=central);
		#head.setText("<b>Players list</b>:");
		#l.addWidget(head);
		self.innerLayout = l;
		
		head = QtGui.QWidget(parent=central);
		headLayout = QtGui.QHBoxLayout();
		headLayout.setMargin(0);
		headLayout.setSpacing(0);
		self.head = head;
		self.headLayout = headLayout;
		self.head.setLayout(self.headLayout);
		l.addWidget(self.head);
		
		self.head1 = QtGui.QWidget(parent=head);
		self.headLayout1 = QtGui.QVBoxLayout();
		self.head1.setLayout(self.headLayout1);
		self.headLayout1.setMargin(0);
		self.headLayout1.setSpacing(0);
		self.headLayout.addWidget(self.head1);
		
		self.head2 = QtGui.QWidget(parent=head);
		self.headLayout2 = QtGui.QVBoxLayout();
		self.head2.setLayout(self.headLayout2);
		self.headLayout2.setMargin(0);
		self.headLayout2.setSpacing(0);
		self.headLayout.addWidget(self.head2);
		
		self.players = PlayerTable(parent=central);
		l.addWidget(self.players);
	
	def generateTeamLabel(self, team, text, right):
		wid = QtGui.QLabel(parent=self.head);
		wid.setTextFormat(QtCore.Qt.PlainText);
		wid.setText(text);
		pal = wid.palette();
		backColor = QtGui.QColor(team.color|0xFF000000);
		pal.setColor(QtGui.QPalette.Window, backColor);
		tbr = checkLightness(backColor);
		if tbr < 128:
			foreColor = QtGui.QColor(0xFFFFFFFF);
		else:
			foreColor = QtGui.QColor(0xFF000000);
		pal.setColor(QtGui.QPalette.WindowText, foreColor);
		wid.setPalette(pal);
		wid.setBackgroundRole(QtGui.QPalette.Window);
		wid.setAutoFillBackground(True);
		wid.setContentsMargins(QtCore.QMargins(3, 3, 3+(3 if right else 0), 3));
		fon = wid.font();
		fon.setBold(True);
		fon.setPointSize(fon.pointSize()+0.5);
		if right:
			wid.setAlignment(QtCore.Qt.AlignRight);
		else:
			wid.setAlignment(QtCore.Qt.AlignLeft);
		wid.setFont(fon);
		return wid;
	
	def show(self, server):
		super(PlayerList, self).hide();
		players = sorted(server.players);
		teams = server.teams;
		
		self.players.setHorizontalHeaderLabels(['', 'Nickname', '%ss' % (server.point_name()), 'Ping', 'Time']);
		
		self.players.server = server;
		self.server = server;
		
		self.players.teams = teams;
		if len(players) == 0:
			self.players.hide();
		else:
			self.players.setRowCount(len(players));
			self.players.show();
		
		if self.headLayout1.count() < 2:
			label_time = QtGui.QLabel(parent=self.head);
			label_time.setTextFormat(QtCore.Qt.PlainText);
			label_time.setText("");
			label_time.setContentsMargins(QtCore.QMargins(3, 0, 3, 3));
			self.label_time = label_time;
			label_timev = QtGui.QLabel(parent=self.head);
			label_timev.setTextFormat(QtCore.Qt.PlainText);
			label_timev.setText("");
			label_timev.setContentsMargins(QtCore.QMargins(3, 0, 6, 3));
			label_timev.setAlignment(QtCore.Qt.AlignRight);
			self.label_timev = label_timev;
			self.headLayout1.addWidget(label_time);
			self.headLayout2.addWidget(label_timev);
			
			label_frags = QtGui.QLabel(parent=self.head);
			label_frags.setTextFormat(QtCore.Qt.PlainText);
			label_frags.setText("");
			label_frags.setContentsMargins(QtCore.QMargins(3, 0, 3, 3));
			self.label_frags = label_frags;
			label_fragsv = QtGui.QLabel(parent=self.head);
			label_fragsv.setTextFormat(QtCore.Qt.PlainText);
			label_fragsv.setText("");
			label_fragsv.setContentsMargins(QtCore.QMargins(3, 0, 6, 3));
			label_fragsv.setAlignment(QtCore.Qt.AlignRight);
			self.label_fragsv = label_fragsv;
			self.headLayout1.addWidget(label_frags);
			self.headLayout2.addWidget(label_fragsv);
		
		if server.timelimit > 0:
			self.label_time.setText('%d minute%s left' % (server.timeleft, 's' if server.timeleft != 1 else ''));
			self.label_time.show();
			self.label_timev.show();
		else:
			self.label_time.hide();
			self.label_timev.hide();
		
		if server.point_name() == 'Frag' and server.fraglimit > 0:
			maxp = 0;
			for player in self.server.players:
				maxp = max(player.points, maxp);
			left = server.fraglimit-maxp;
			self.label_frags.setText('%d frag%s left' % (left, 's' if left != 1 else ''));
			self.label_frags.show();
			self.label_fragsv.show();
		elif server.point_name() == 'Flag' and server.pointlimit > 0:
			maxp = 0;
			for player in self.server.players:
				maxp = max(player.points, maxp);
			left = server.pointlimit-maxp;
			self.label_frags.setText('%d point%s left' % (left, 's' if left != 1 else ''));
			self.label_frags.show();
			self.label_fragsv.show();
		else:
			self.label_frags.hide();
			self.label_fragsv.hide();
		
		while self.headLayout1.count() > 2:
			wid = self.headLayout1.takeAt(2);
			wid.widget().deleteLater();
		while self.headLayout2.count() > 2:
			wid = self.headLayout2.takeAt(2);
			wid.widget().deleteLater();
		
		for team in self.players.teams:
			self.headLayout1.addWidget(self.generateTeamLabel(team, team.name, False));
			self.headLayout2.addWidget(self.generateTeamLabel(team, ('%d / %d' % (team.score, server.pointlimit)) if server.pointlimit > 0 else str(team.score), True));
		
		self.headLayout1.invalidate();
		self.headLayout1.activate();
		self.headLayout2.invalidate();
		self.headLayout2.activate();
		self.headLayout.invalidate();
		self.headLayout.activate();
		
		_i = 0;
		if len(self.players.teams) > 0:
			for i in range(0, len(self.players.teams)):
				for player in players:
					if player.spectator or player.team != i:
						continue;
					self.players.createRow(_i, player);
					_i += 1;
		else:
			for player in players:
				if player.spectator:
					continue;
				self.players.createRow(_i, player);
				_i += 1;
		for player in players:
			if not player.spectator:
				continue;
			self.players.createRow(_i, player);
			_i += 1;
		self.innerLayout.invalidate();
		self.innerLayout.activate();
		self.layout.invalidate();
		self.layout.activate();
		self.resize(self.sizeHint());
		desktop = QtGui.QDesktopWidget();
		deskSize = desktop.availableGeometry(desktop.primaryScreen()); # this may be buggy if your 1st monitor is smaller than your 2nd monitor
		geometry = self.geometry();
		if geometry.bottom() > deskSize.bottom():
			self.move(geometry.x(), deskSize.bottom()-geometry.height());
		super(PlayerList, self).show();

g_PlayerList = None;