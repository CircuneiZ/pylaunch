# this script was made due to the original GeoIP binary database reader being overcomplicated (geez, these guys love backward compatibility and storage of data in the source code)
# basically it takes original MaxMind's CSV version of the database and converts it to binary format.
#  - original CSV size: ~6mb
#  - output raw binary size: ~1mb
#  - output gzipped binary size: ~400kb (!!!LESS!!! in size than original MaxMind's database)

import struct;
import gzip;
import sys;

class IP2Country:
	def __init__(self):
		self.countries = [];
		self.ranges = [];
	
	def locate_country(self, code):
		_i = 0;
		for country in self.countries:
			if country[0] == code:
				return _i;
			_i += 1;
		return -1;
	
	def load_from_csv(self, csvname):
		try:
			buf = '';
			with open(csvname, "r") as _in:
				buf = _in.read();
			buf = buf.split('\n');
			for row in buf:
				if len(row) <= 0:
					continue;
				if row[0] == '#':
					continue;
				row = row.replace('\"', '').split(',');
				country = self.locate_country(row[4]);
				if country == -1:
					country = [str(row[4]), str(row[5])];
					self.countries.append(country);
					country = len(self.countries)-1;
				Range = [int(row[2]), int(row[3]), country];
				self.ranges.append(Range);
			return True;
		except:
			self.countries = [];
			self.ranges = [];
			return False;
	
	def load_from_dat(self, datname):
		try:
			buf = b'';
			with gzip.GzipFile(datname, "rb") as _in:
				buf = _in.read();
			pos = 0;
			len_countries = struct.unpack('<L', buf[pos:pos+4])[0];
			pos += 4;
			self.countries = [];
			for i in range(0, len_countries):
				_ccode = '';
				_cname = '';
				for c in range(pos, len(buf)):
					if buf[c] == 0:
						pos += 1;
						break;
					_ccode += chr(buf[c]);
					pos += 1;
				for c in range(pos, len(buf)):
					if buf[c] == 0:
						pos += 1;
						break;
					_cname += chr(buf[c]);
					pos += 1;
				country = [_ccode, _cname];
				self.countries.append(country);
			len_ranges = struct.unpack('<L', buf[pos:pos+4])[0];
			pos += 4;
			self.ranges = [];
			for i in range(0, len_ranges):
				r_from, r_to, r_country = struct.unpack('<LLL', buf[pos:pos+12]);
				country = [r_from, r_to, r_country];
				self.ranges.append(country);
				pos += 12;
			return True;
		except:
			self.countries = [];
			self.ranges = [];
			return False;
	
	def save_to_dat(self, datname):
		try:
			outbuf = b'';
			outbuf += struct.pack('<L', len(self.countries));
			for country in self.countries:
				c0 = country[0].encode('utf-8');
				c1 = country[1].encode('utf-8');
				outbuf += struct.pack('<%ds%ds'%(len(c0)+1, len(c1)+1), c0, c1);
			outbuf += struct.pack('<L', len(self.ranges));
			for Range in self.ranges:
				outbuf += struct.pack('<LLL', Range[0], Range[1], Range[2]);
			with gzip.GzipFile(datname, "wb") as _out:
				_out.write(outbuf);
			return True;
		except:
			return False;
	
	def ip_to_octets(self, ip):
		ip = ip.split('.');
		if len(ip) != 4:
			return None;
		oct_1 = int(ip[0]);
		oct_2 = int(ip[1]);
		oct_3 = int(ip[2]);
		oct_4 = int(ip[3]);
		oip = oct_1; oip <<= 8;
		oip |= oct_2; oip <<= 8;
		oip |= oct_3; oip <<= 8;
		oip |= oct_4;
		return oip;
	
	def resolve_ip(self, ip):
		if isinstance(ip, (str)):
			ip = self.ip_to_octets(ip);
		if ip is None:
			return None;
		for Range in self.ranges:
			r_from = Range[0];
			r_to = Range[1];
			r_country = Range[2];
			if (ip >= r_from) and (ip <= r_to):
				if r_country < len(self.countries):
					return self.countries[r_country];
				return None;
		return None;
	
	def free(self):
		self.countries = [];
		self.ranges = [];

# Steps to update the DB:
#  - download and unpack: http://geolite.maxmind.com/download/geoip/database/GeoIPCountryCSV.zip
#  - feed the GeoIPCountryWhois.csv to convert_csv_to_dat function

# Using the DB:
#  - instantiate IP2Country file
#  - load either CSV (large, slow, not recommended) or own database:
#     IP2Country.load_from_csv
#     IP2Country.load_from_dat
#  - use IP2Country.resolve_ip method. it returns either None or [code, name] for the country.

def convert_csv_to_dat(csv):
	ip2c = IP2Country();
	if not ip2c.load_from_csv(csv):
		return False;
	ip2c.save_to_dat("%s/ip2country/GeoIPCountryWhois.dat" % (sys.mybase));
	ip2c.free();
