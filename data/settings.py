from PyQt4 import QtCore;

import json;

_supportedSettings = ["pyl.browser.master_address",
						"pyl.browser.master_port",
						"pyl.browser.master_use",
						"pyl.browser.timeout_master",
						"pyl.browser.timeout_servers",
						"pyl.browser.sort_column",
						"pyl.browser.sort_order",
						"pyl.browser.sort_players",
						"pyl.browser.ping_good", # below this value pings are green
						"pyl.browser.ping_bad",  # below this value pings are yellow and above this value pings are red
						"pyl.browser.refresh_animated",
						
						"pyl.wad.searching", # URLs to search for PWADs in
						"pyl.wad.saving", # local directories to look for PWADs in
						];

# just init it first time
_qsettings = QtCore.QSettings("AlienTech", "PyLaunch");

# defaults
_settings = {"pyl.browser.master_address": "master.zandronum.com",
				"pyl.browser.master_port": 15300,
				"pyl.browser.master_use": True,
				"pyl.browser.timeout_master": 2000,
				"pyl.browser.timeout_servers": 4000,
				"pyl.browser.sort_column": 8,
				"pyl.browser.sort_order": 1, # asc
				"pyl.browser.sort_players": True,
				"pyl.browser.ping_good": 100,
				"pyl.browser.ping_bad": 200,
				"pyl.browser.refresh_animated": False,
				"pyl.wad.searching": ["http://static.best-ever.org/wads/",
										"http://inferno.in.ua/wadhost/", # super fucking secret wadhost.fathax.com mirror :)
										"http://sickedwick.net/wads/",
										"http://zandronum.net/wads/",
										"http://doom.tc/wads/",
										"http://files.funcrusherplus.net/wads/",
										"http://www.grandpachuck.org/files/wads/",
										"http://oblacek.sh.cvut.cz/bin/",
										"http://doom.dogsoft.net/getwad.php?search=",
										"http://wads.coffeenet.org/zips/",
										"http://wadhost.fathax.com/files/getwad.php?",
										"http://wadhost.fathax.com/e107_files/downloads/",
										"http://www.wadtemp.fathax.com/files/"
										],
				"pyl.wad.saving": ["."]};

def Load():
	for setting in _supportedSettings:
		if setting in _settings:
			s = _settings[setting];
		else:
			s = None;
		_settings[setting] = _qsettings.value(setting);
		if _settings[setting] is None:
			_settings[setting] = s;
		else:
			_settings[setting] = json.loads(_settings[setting]);

def Get(name):
	if name not in _settings:
		return None;
	return _settings[name];

def GetInt(name):
	v = Get(name);
	if v is None:
		return None;
	try:
		v = int(v);
		return v;
	except:
		return 0;

def GetBool(name):
	v = Get(name);
	if v is None:
		return None;
	try:
		v = (str(v).lower() == 'true');
		return v;
	except:
		return False;

def GetFloat(name):
	v = Get(name);
	if v is None:
		return None;
	try:
		v = float(v);
		return v;
	except:
		return False;

def Set(name, value): # "set" is a keyword and raises an error (wtf python?)
	___s = json.dumps(value);
	_settings[name] = value;
	_qsettings.setValue(name, ___s);

Load();
